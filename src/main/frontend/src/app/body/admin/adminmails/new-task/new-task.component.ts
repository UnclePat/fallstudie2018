import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

import { PersonService, Person } from 'src/app/services/person.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialogClose } from '@angular/material';
import { Task, TaskService, Priority, Status } from 'src/app/services/task.service';
import { ProjectService, Project } from 'src/app/services/project.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-new-task',
  templateUrl: './new-task.component.html',
  styleUrls: ['./new-task.component.css']
})
export class NewTaskComponent implements OnInit {

  constructor(private personService: PersonService,
    private projectService: ProjectService,
    private taskService: TaskService,
    private userService: UserService,
    public dialogRef: MatDialogRef<NewTaskComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { 
  }

  title = new FormControl(this.data.mail.subject, [Validators.required]);
  responsible = new FormControl('', [Validators.required]);
  project = new FormControl('');
  deadline = new FormControl('', [Validators.required]);
  priority = new FormControl('', [Validators.required]);
  status = new FormControl('', [Validators.required]);
  description = new FormControl('', [Validators.required]);
  expectedExpanse = new FormControl('');
  
  members: Person[];
  priorities: Priority[];
  taskStatus: Status[];
  projects: Project[];
  newTask = new Task();
  setMember: any;
  setStatus: any;
  setPriority: any;
  setProject: any;

  ngOnInit() {
    this.getAllUsers();
    this.getPriorities();
    this.getStatus();
    this.getAllProjects();
  }

  getAllProjects() {
    this.projectService.getProjects()
      .subscribe(projects => this.projects = projects);
  }

  changeProject() {
    this.personService.getAllProjectMembers(this.setProject.id)
      .subscribe(members => this.members = members);
    console.log(this.setProject.id);
  }

  getAllUsers() {
    this.userService.getAllUsers()
      .subscribe(members => this.members = members);
  }

  getPriorities() {
    this.taskService.getPriorities()
      .subscribe(priorities => this.priorities = priorities);
  }

  getStatus() {
    this.taskService.getStatus()
      .subscribe(tasksStatus => this.taskStatus = tasksStatus);
  }

  getAllTeamMembers () {
    this.personService.getAllProjectMembers(this.data.project.id)
      .subscribe(members => this.members = members);
  }

  addTask() {

    this.newTask.title = this.title.value;
    this.newTask.responsible = this.setMember;
    this.newTask.projectId = this.setProject.id;
    this.newTask.deadline = this.deadline.value;
    this.newTask.priority = this.setPriority;
    this.newTask.status = this.setStatus;
    this.newTask.description = this.description.value;
    this.newTask.expectedExpense = this.expectedExpanse.value;
    this.newTask

    console.log(this.newTask );
    
    this.taskService.addTask(this.newTask, this.setProject.id)
    .subscribe(
      result => console.log(result)
    );

    this.title.setValue("");
    this.responsible.setValue("");
    this.deadline.setValue("");
    this.priority.setValue("");
    this.status.setValue("");
    this.description.setValue("");
  }
}