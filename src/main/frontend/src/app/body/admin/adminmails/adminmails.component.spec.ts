import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminmailsComponent } from './adminmails.component';

describe('AdminmailsComponent', () => {
  let component: AdminmailsComponent;
  let fixture: ComponentFixture<AdminmailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminmailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminmailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
