import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnswerMailComponent } from './answer-mail.component';

describe('AnswerMailComponent', () => {
  let component: AnswerMailComponent;
  let fixture: ComponentFixture<AnswerMailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnswerMailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnswerMailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
