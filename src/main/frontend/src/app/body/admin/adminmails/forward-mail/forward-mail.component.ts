import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl } from '@angular/forms';
import { MailService, SendMail } from 'src/app/services/email.service';
import { parseConfigFileTextToJson } from 'typescript';

@Component({
  selector: 'app-forward-mail',
  templateUrl: './forward-mail.component.html',
  styleUrls: ['./forward-mail.component.css']
})
export class ForwardMailComponent implements OnInit {
mailContent= new FormControl();
sendTo = new FormControl();

forwardMail= new SendMail();


  constructor(public dialogRef: MatDialogRef<ForwardMailComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private mailService: MailService) { }

  ngOnInit() {
  }

  sendMail(){
    this.forwardMail.content = this.mailContent.value;
    this.forwardMail.to = this.sendTo.value;
    this.forwardMail.subject = this.data.mai.subject;

    console.log(this.forwardMail);
    console.log(this.data);
    
    this.mailService.sendMail(this.forwardMail)
      .subscribe(
        result => console.log(result)
      );
  }
}
