import { Component, OnInit, ViewChild } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatDialog, MatDialogConfig, MatTableDataSource,MatSort } from '@angular/material';
import { NewTaskComponent } from './new-task/new-task.component';
import { Email, MailService } from 'src/app/services/email.service';
import { AnswerMailComponent } from './answer-mail/answer-mail.component';
import { ForwardMailComponent } from './forward-mail/forward-mail.component';
import { NewMailComponent } from 'src/app/shared/popups/newmail/new-mail.component';
import { ReallyDeleteComponent } from 'src/app/shared/popups/really-delete/really-delete.component';
import { DataSource } from '@angular/cdk/table';


@Component({
  selector: 'app-adminmails',
  templateUrl: './adminmails.component.html',
  styleUrls: ['./adminmails.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class AdminmailsComponent implements OnInit {

  dataSource: any;
  columnsToDisplay = ['subject', 'timestamp', 'sender',];
  expandedElement: Email | null;
  @ViewChild(MatSort) sort: MatSort;

  //mail: Email[] = [];
  Email = [];

  constructor(private mailService: MailService, public dialog: MatDialog) { 
  }

  ngOnInit() {
    this.getAllMails();
    setTimeout(() => {
      return this.dataSource.sort = this.sort;
    },4000);
  }


  
  

  getAllMails() {
    this.mailService.getAllMails()
    .subscribe((data) => {this.dataSource = new MatTableDataSource(data);}
    ) 
  }
  openNewTask (mail: Email) {
    this.dialog.open(NewTaskComponent, {data: {mail: mail}});
  }

  openReallyDelete(mail:Email){
    //Übergabe Daten von ausgewählter Mail an Componente übergeben
    this.dialog.open(ReallyDeleteComponent, { data: { mai: mail }  });
  }

  answerMail(email:Email) {
   // const dialogConfig = new MatDialogConfig();
    this.dialog.open(AnswerMailComponent, {data:{mai: email}});
  }

  forwardMail (email: Email) {
   // console.log('mail' + email);
    this.dialog.open(ForwardMailComponent, {data: { mai: email}});
  }

  newMail(){
   const dialogConfig = new MatDialogConfig();
   this.dialog.open(NewMailComponent,dialogConfig);
  }
}
