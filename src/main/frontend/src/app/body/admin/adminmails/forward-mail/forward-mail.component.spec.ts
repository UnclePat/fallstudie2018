import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForwardMailComponent } from './forward-mail.component';

describe('ForwardMailComponent', () => {
  let component: ForwardMailComponent;
  let fixture: ComponentFixture<ForwardMailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForwardMailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForwardMailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
