import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatPaginator} from '@angular/material';
import { FormControl } from '@angular/forms';
import { SendMail, MailService } from 'src/app/services/email.service';

@Component({
  selector: 'app-answer-mail',
  templateUrl: './answer-mail.component.html',
  styleUrls: ['./answer-mail.component.css']
})
export class AnswerMailComponent implements OnInit {
  mailContent = new FormControl();
  newMail= new SendMail();

  constructor(public dialogRef: MatDialogRef<AnswerMailComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private emailService: MailService) { }

  ngOnInit() {
  }
  onNoClick(){
    window.close();
  }


  sendMail(){
    this.newMail.content = this.mailContent.value;
    this.newMail.subject = this.data.mai.subject;
    this.newMail.to = this.data.mai.sender.split('<').pop().split('>')[0];

    //console.log(this.newMail);

    this.emailService.sendMail(this.newMail)
    .subscribe(
      result => console.log(result)
    );
    
  }
}
