import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialogConfig, MatTableDataSource, MatDialog, MatSort } from '@angular/material';
import { ReallyDeleteComponent } from 'src/app/shared/popups/really-delete/really-delete.component';
import { Person, PersonService } from 'src/app/services/person.service';
import { ChangecompanyComponent } from '../changecompany/changecompany.component';
import { NewcompanyComponent } from '../newcompany/newcompany.component';
import { Customer, CustomerService } from 'src/app/services/customer.service';

@Component({
  selector: 'app-admincompanies',
  templateUrl: './admincompanies.component.html',
  styleUrls: ['./admincompanies.component.css']
})
export class AdmincompaniesComponent implements OnInit {


  @ViewChild(MatSort) sort: MatSort;

  displayedColumns: string[] = ['id', 'name', 'adresse', 'e-mail', 'telefon', 'website', 'bearbeiten', 'loeschen'];
  dataSource: any;

  customers: Customer[] = [];

  constructor(private customerService: CustomerService, 
    public dialog: MatDialog) {
    //this.dataSource.sort = this.sort;
  }
  /*
  applyFilter(filterValue: string) {
      this.dataSource.filter = filterValue.trim().toLowerCase();
  }*/

  
  ngOnInit() {
    this.getAllCustomers();
    setTimeout(() => {
      return this.dataSource.sort = this.sort;
    },300);
 }

  getAllCustomers(): void {
    this.customerService.getAllCustomers()
      .subscribe((data) => {this.dataSource = new MatTableDataSource(data);}
      )
  }

  openReallyDelete (customer: Customer) {
    //onst dialogConfig = new MatDialogConfig();
    this.dialog.open(ReallyDeleteComponent, {data: { cus: customer }});
  }

  
  openChangecompany (customer: Customer) {
    
     //const dialogConfig = new MatDialogConfig();
    this.dialog.open(ChangecompanyComponent, {data: {cus: customer}});
  }

  openNewcompany () {
    const dialogConfig = new MatDialogConfig();
    this.dialog.open(NewcompanyComponent, dialogConfig);
  }
}
