import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatTableDataSource, MatSort, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { CustomerService, Customer } from 'src/app/services/customer.service';
import { templateJitUrl } from '@angular/compiler';

@Component({
  selector: 'app-changecompany',
  templateUrl: './changecompany.component.html',
  styleUrls: ['./changecompany.component.css']
})
export class ChangecompanyComponent implements OnInit {

  email = new FormControl('', [Validators.required, Validators.email]);
  telefon = new FormControl('', [Validators.required]);
  website = new FormControl('', [Validators.required]);
  ustid = new FormControl('', [Validators.required]);
  kundenname = new FormControl('', [Validators.required]);


  getErrorMessageTelefon() {
    return this.telefon.hasError('required') ? 'Bitte geben Sie Ihre Telefonnummer ein' :
      this.telefon.hasError('nachname') ? 'Bitte geben Sie Ihre Telefonnummer ein' :
        '';
  }


  getErrorMessageEMail() {
    return this.email.hasError('required') ? 'Bitte geben Sie eine gültige E-Mail Adresse ein' :
      this.email.hasError('email') ? 'Bitte geben Sie eine gültige E-Mail Adresse ein' :
        '';
  }

  getErrorMessageWebsite() {
    return this.website.hasError('required') ? 'Bitte geben Sie eine gültige Website ein' :
      this.website.hasError('email') ? 'Bitte geben Sie eine gültige Website Adresse ein' :
        '';
  }

  getErrorMessageUstId() {
    return this.ustid.hasError('required') ? 'Bitte geben Sie Ihre Umsatzsteuer Identifikationsnummer ein' :
      this.ustid.hasError('ustid') ? 'Bitte geben Sie Ihre Umsatzsteuer Identifikationsnummer ein' :
        '';
  }

  getErrorMessageKundenname() {
    return this.kundenname.hasError('required') ? 'Bitte geben Sie Ihre Telefonnummer ein' :
      this.kundenname.hasError('kundenname') ? 'Bitte geben Sie Ihre Telefonnummer ein' :
        '';
  }


  //TableSorting
  @ViewChild(MatSort) sort: MatSort;
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  constructor(private customerService: CustomerService,
    public dialogRef: MatDialogRef<ChangecompanyComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {

    //console.log('data passed in dialog:', this.data);
    this.dataSource = new MatTableDataSource();
  }

  ngOnInit() {
    this.dataSource.data = ELEMENT_DATA;
    this.dataSource.sort = this.sort;

  }

  columnsToDisplay = ['EMailAdresse', 'Telefon', 'Website', 'UstId', 'Kundenname'];
  expandedElement: PeriodicElement | null;

  customer: Customer;



  saveChangedCompany() {
    this.customerService.updateCustomer(this.data.cus, this.data.cus.id)
      .subscribe();
    console.log('save company');
  }

}

export interface PeriodicElement {

  email: string;
  telefon: number;
  website: string;
  ustid: number;
  kundenname: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {

    email: 'Max@Mustermann.de',
    telefon: 11223344,
    website: 'www.Unternehmen.de',
    ustid: 1234,
    kundenname: 'maxi'
  }];