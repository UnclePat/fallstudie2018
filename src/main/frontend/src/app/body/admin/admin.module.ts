import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminNewprojectComponent } from './admin-newproject/admin-newproject.component';
import { AdminmailsComponent } from './adminmails/adminmails.component';
import { AdminusersComponent } from './adminusers/adminusers.component';
import { NewTaskComponent } from './adminmails/new-task/new-task.component';
import { NewUserComponent } from './adminusers/new-user/new-user.component';
import { ChangeUserComponent } from './adminusers/change-user/change-user.component';
import { MatFormFieldModule, MatInputModule, MatSelectModule, MatIconModule, MatRadioModule, MatTableModule, MatSortModule, MatDialogModule, MatDatepickerModule, MatCheckboxModule, MatPaginatorModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ForwardMailComponent } from './adminmails/forward-mail/forward-mail.component';
import { AnswerMailComponent } from './adminmails/answer-mail/answer-mail.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import { AdmincompaniesComponent } from './admincompanies/admincompanies.component';
import { NewcompanyComponent } from './newcompany/newcompany.component';
import { ChangecompanyComponent } from './changecompany/changecompany.component';
import { NewMailComponent } from 'src/app/shared/popups/newmail/new-mail.component';

@NgModule({
  imports: [
    MatSortModule,
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatRadioModule,
    MatTableModule,
    MatDialogModule,
    MatDatepickerModule,
    MatCheckboxModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatPaginatorModule,
    


  ],
  declarations: [
    AdminNewprojectComponent,
    AdminmailsComponent,
    AdminusersComponent,
    NewTaskComponent,
    NewUserComponent,
    ChangeUserComponent,
    ForwardMailComponent,
    AnswerMailComponent,
    AdmincompaniesComponent,
    NewcompanyComponent,
    ChangecompanyComponent
  ],

  exports: [
    NewUserComponent,
  
  ],

  entryComponents: [
    ForwardMailComponent, 
    AnswerMailComponent,
    NewTaskComponent,
    NewMailComponent,
    ChangeUserComponent
  ]

})
export class AdminModule { }
