import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatTableDataSource, MatSort } from '@angular/material';
import { Customer, CustomerService } from 'src/app/services/customer.service';

@Component({
  selector: 'app-newcompany',
  templateUrl: './newcompany.component.html',
  styleUrls: ['./newcompany.component.css']
})
export class NewcompanyComponent implements OnInit {
 customer = new FormControl('', [Validators.required]);
 street = new FormControl();
 housenumber = new FormControl();
 zipcode = new FormControl();
 location = new FormControl();

  email = new FormControl('', [Validators.required, Validators.email]);
  phone = new FormControl('', [Validators.required]);
  website = new FormControl('', [Validators.required]);
  ustid = new FormControl('', [Validators.required]);
 iban = new FormControl();
 bic = new FormControl();


  getErrorMessageTelefon() {
    return this.phone.hasError('required') ? 'Bitte geben Sie Ihre Telefonnummer ein' :
      this.phone.hasError('nachname') ? 'Bitte geben Sie Ihre Telefonnummer ein' :
    '';}

      
  getErrorMessageEMail() {
    return this.email.hasError('required') ? 'Bitte geben Sie eine gültige E-Mail Adresse ein' :
      this.email.hasError('email') ? 'Bitte geben Sie eine gültige E-Mail Adresse ein' :
    '';}

    getErrorMessageWebsite() {
      return this.website.hasError('required') ? 'Bitte geben Sie eine gültige Website ein' :
        this.website.hasError('email') ? 'Bitte geben Sie eine gültige Website Adresse ein' :
      '';}

      getErrorMessageUstId() {
        return this.ustid.hasError('required') ? 'Bitte geben Sie Ihre Umsatzsteuer Identifikationsnummer ein' :
          this.ustid.hasError('ustid') ? 'Bitte geben Sie Ihre Umsatzsteuer Identifikationsnummer ein' :
        '';}

        getErrorMessageKundenname() {
          return this.customer.hasError('required') ? 'Bitte geben Sie Ihre Telefonnummer ein' :
            this.customer.hasError('nachname') ? 'Bitte geben Sie Ihre Telefonnummer ein' :
          '';}
      



    //TableSorting
    @ViewChild(MatSort) sort: MatSort;
    dataSource = new MatTableDataSource(ELEMENT_DATA);
  constructor(private customerService: CustomerService){
    this.dataSource = new MatTableDataSource();
  }

  ngOnInit() {
    this.dataSource.data = ELEMENT_DATA;
    this.dataSource.sort = this.sort;
  }

  newCompany = new Customer();

  addNewCompany(){

    this.newCompany.name = this.customer.value;
    this.newCompany.street = this.street.value;
    this.newCompany.housenumber = this.housenumber.value;
    this.newCompany.zipcode = this.zipcode.value;
    this.newCompany.location = this.location.value;
    this.newCompany.phone = this.phone.value;
    this.newCompany.email = this.email.value;
    this.newCompany.website = this.website.value;
    this.newCompany.taxid = this.ustid.value;
    this.newCompany.iban = this.iban.value;
    this.newCompany.bic = this.bic.value;

    console.log(this.newCompany );
    
    this.customerService.addCustomer(this.newCompany)
    .subscribe(
      result => console.log(result)
    );

  }

  
  columnsToDisplay = ['EMailAdresse', 'Telefon', 'Website', 'UstId', 'Kundenname'];
  expandedElement: PeriodicElement | null;

}

export interface PeriodicElement{

email: string;
telefon: number;
website: string;
ustid: number;
kundenname: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {
  
    email: 'Max@Mustermann.de',
    telefon: 11223344,
    website: 'www.Unternehmen.de',
    ustid: 1234,
    kundenname: 'maxi'
  }];