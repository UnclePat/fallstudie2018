import { Component, OnInit, Inject, Input } from '@angular/core';
import { UserService, User } from 'src/app/services/user.service';
import { ActivatedRoute } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AdminusersComponent } from '../adminusers.component';
import { CustomerService } from 'src/app/services/customer.service';


export interface Benutzer {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-change-user',
  templateUrl: './change-user.component.html',
  styleUrls: ['./change-user.component.css']
})

export class ChangeUserComponent implements OnInit {
  //name;
  //selectionModel;
  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    public dialogRef: MatDialogRef<ChangeUserComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  //Check Radio Button wenn Kunde oder Mitarbeiter
  get isCustomer() {
    //console.log(this.data.us.roles[0].name);
    if (this.data.us.roles[0].name === "Kunde") {
      return true;
    }
    return false;
  }
  get isEmployee() {
   // console.log(this.data.us.roles[0].name);
    if (this.data.us.roles[0].name === "Mitarbeiter") {
      return true;
    }
    return false;
  }

  ngOnInit(): void {

  }

  benutzers: Benutzer[] = [
    { value: 'JaHö', viewValue: 'Jan-David Hötzel' },
    { value: 'LuAs', viewValue: 'Lukas Ascher' },
  ];

  saveChangedUser() {
    this.userService.updateUser(this.data.us)//.us[0])
      .subscribe();
  }


  /*onChange(){
    this.name = this.selectionModel.benutzername;
  }*/


}

