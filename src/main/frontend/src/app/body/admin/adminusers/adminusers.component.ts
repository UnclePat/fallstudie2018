import { Component, OnInit, ViewChild } from '@angular/core';
import { Person, PersonService } from 'src/app/services/person.service';
import { MatTableDataSource, MatSort } from '@angular/material';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { ReallyDeleteComponent } from 'src/app/shared/popups/really-delete/really-delete.component';
import { NewUserComponent } from 'src/app/body/admin/adminusers/new-user/new-user.component';
import { ChangeUserComponent } from './change-user/change-user.component';
import { User, UserService } from 'src/app/services/user.service';
import { DataSource } from '@angular/cdk/table';
//import { User } from '../../../services/user.service';

@Component({
  selector: 'app-adminusers',
  templateUrl: './adminusers.component.html',
  styleUrls: ['./adminusers.component.css'],
  providers: [PersonService]
})
export class AdminusersComponent implements OnInit {

  @ViewChild(MatSort) sort: MatSort;

  displayedColumns: string[] = ['id', 'firstname', 'lastname', 'mail', 'phone', 'bearbeiten', 'loeschen'];
  dataSource: any;
  //public dataSource = new MatTableDataSource<User>();
  //people: User[] = [];
  User = [];
  
  selectedPerson: any;
  clickSort: boolean;

  dataService: any;

  constructor(private userService: UserService, public dialog: MatDialog) { 
  }

  ngOnInit() {
    this.getAllUsers();
    setTimeout(() => {
      return this.dataSource.sort = this.sort;
    },500);
    //this.dataSource.sort = this.sort;
    
  }

  openReallyDelete (user: User ) {
    //const dialogConfig = new MatDialogConfig();
    this.dialog.open(ReallyDeleteComponent, {data: {us: user}});
  }

  //Methode befüllen, wenn klar ist wie change-user umgesetzt wird (popup oder direkt in der Tabelle)
  openChangeUser (user: User) {
    //console.log(user + 'selectedUser');
    this.dialog.open(ChangeUserComponent, { data: { us: user }  });
  }

  openNewUser () {
    const dialogConfig = new MatDialogConfig();
    this.dialog.open(NewUserComponent, dialogConfig);
  }
  getAllUsers(): void {
    this.userService.getAllUsers()
      .subscribe((data) => {this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort}
      )
  }
}
