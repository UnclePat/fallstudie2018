import { Component, OnInit, ViewChild } from '@angular/core';

import { FormControl, Validators, FormBuilder, FormGroup } from '@angular/forms';
import { MatSort, MatTableDataSource, MatDialogRef } from '@angular/material';
import { User, UserService } from 'src/app/services/user.service';
import { CustomerService, Customer } from 'src/app/services/customer.service';
import { AdminusersComponent } from '../adminusers.component';

export interface Rsole{
  id:Number,
  name: String,
  descrption: String
}


@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent implements OnInit {
benutzername = new FormControl('', [Validators.required]); 
vorname = new FormControl('', [Validators.required]); 
nachname = new FormControl('', [Validators.required]);
email = new FormControl('', [Validators.required, Validators.email]);
emailbestaetigen = new FormControl('', [Validators.required, Validators.email]);
neuesPasswort = new FormControl('', [Validators.required]); 
neuesPasswortbestaetigen = new FormControl('', [Validators.required]); 
telefon = new FormControl('', [Validators.required]); 
mobile = new FormControl('', [Validators.required]); 
street = new FormControl();
housenumber = new FormControl();
location = new FormControl();
zipcode = new FormControl();
customer = new FormControl();
role= new FormControl();

getErrorMessageBenutzername() {
  return this.vorname.hasError('required') ? 'Bitte geben Sie Ihren Benutzernamen ein' :
    this.vorname.hasError('benutzername') ? 'Bitte geben Sie Ihren Benutzernamen ein' :
  '';}


getErrorMessageVorname() {
  return this.vorname.hasError('required') ? 'Bitte geben Sie Ihren Vornamen ein' :
    this.vorname.hasError('vorname') ? 'Bitte geben Sie Ihren Vornamen ein' :
  '';}

  getErrorMessageNachname() {
    return this.nachname.hasError('required') ? 'Bitte geben Sie Ihren Nachnamen ein' :
      this.nachname.hasError('nachname') ? 'Bitte geben Sie Ihren Nachnamen ein' :
    '';}

    
getErrorMessageEMail() {
  return this.email.hasError('required') ? 'Bitte geben Sie eine gültige E-Mail Adresse ein' :
    this.email.hasError('email') ? 'Bitte geben Sie eine gültige E-Mail Adresse ein' :
  '';}

getErrorMessageEMailBestaetigen() {
  return this.emailbestaetigen.hasError('required') ? 'Bitte bestätigen Sie Ihre E-Mail Adresse' :
    this.emailbestaetigen.hasError('emailbestaetigen') ? 'Bitte bestätigen Sie Ihre E-Mail Adresse' :
  '';}

getErrorMessageNeuesPasswort() {
  return this.neuesPasswort.hasError('required') ? 'Bitte geben Sie ein neues Passwort ein' :
    this.neuesPasswort.hasError('neuesPasswort') ? 'Bitte geben Sie ein neues Passwort ein' :
  '';}

getErrorMessageNeuesPasswortBestaetigen() {
  return this.neuesPasswortbestaetigen.hasError('required') ? 'Bitte bestätigen Sie Ihr neues Passwort' :
    this.neuesPasswortbestaetigen.hasError('neuesPasswortbestaetigen') ? 'Bitte bestätigen Sie Ihr neues Passwort' :
  ''; }

getErrorMessageTelefon() {
  return this.telefon.hasError('required') ? 'Bitte geben Sie Ihre Telefonnummer ein' :
    this.telefon.hasError('nachname') ? 'Bitte geben Sie Ihre Telefonnummer ein' :
  '';}

getErrorMessageHandy() {
  return this.mobile.hasError('required') ? 'Bitte geben Sie Ihre Handynummer ein' :
    this.mobile.hasError('nachname') ? 'Bitte geben Sie Ihre Handynummer ein' :
  '';}



  constructor(private form: FormBuilder,
    private customerService: CustomerService,
    private userService: UserService,
    private dialogRef:MatDialogRef<NewUserComponent>){
  this.dataSource = new MatTableDataSource();

  }


  hide = true;

    //TableSorting
    @ViewChild(MatSort) sort: MatSort;
    dataSource = new MatTableDataSource(ELEMENT_DATA);
 


  ngOnInit() {
    this.dataSource.data = ELEMENT_DATA;
    this.dataSource.sort = this.sort;
    this.getAllCustomers();
  }

  columnsToDisplay = ['Benutzername', 'Vorname', 'Nachname','EMailAdresse', 'EMailAdressebestaetigen', 'NeuesPasswort', 'NeuesPasswortbestaetigen', 'Straße', 'Hausnummer', 'Ort', 'Postleitzahl', 'Telefon', 'Handy'];
  expandedElement: PeriodicElement | null;

  newUser= new User();
  setRole: any;
  setCustomer: any;
  roles = [{id: '2', name:'Mitarbeiter', }, {id:'3', name: 'Kunde'}];
  customers: Customer[];
  
  


  saveUser(){
    this.newUser.username = this.benutzername.value;
    this.newUser.firstname = this.vorname.value;
    this.newUser.lastname = this.nachname.value;
    this.newUser.street = this.street.value;
    this.newUser.housenumber = this.housenumber.value;
    this.newUser.place = this.location.value;
    this.newUser.zipcode = this.zipcode.value;
    this.newUser.phone = this.telefon.value;
    this.newUser.mobil = this.mobile.value;
    this.newUser.enterprise = this.setCustomer;

    let x: [{id: number, description: string, name: string}];
    
    if(this.setRole == 2){
      x = [{ id: 2 , name: 'Mitarbeiter', description: ''}];
      this.newUser.roles = x;
      //console.log(this.newUser);

    }else if(this.setRole == 3){

      x = [{ id: 3 , name: 'Kunde', description: ''}];
      this.newUser.roles = x;
      //console.log(this.newUser);

    }else {
      console.log('No user role defined!');
    }

    
    if((this.email.value != null || this.emailbestaetigen.value != null ) && this.email.value == this.emailbestaetigen.value){
      this.newUser.email = this.email.value;
    }else{ console.log('mail nicht übereinstimmend');}

    if(this.neuesPasswort.value != null || this.neuesPasswortbestaetigen.value != null && this.neuesPasswort.value == this.neuesPasswortbestaetigen.value){
      
      this.newUser.password = this.neuesPasswort.value;
    }else{ console.log('pw nicht übereinstimmend');}

    
   // console.log(this.newUser);  
      this.userService.addUser(this.newUser)
     .subscribe(
      result => console.log(result)
    );   

  
    setTimeout(() => {
      this.dialogRef.close();
      window.location.reload();
    }, 10);

  }

  getAllCustomers(){
    this.customerService.getAllCustomers()
    .subscribe(customers => this.customers = customers);
  }

  
}


export interface PeriodicElement{
benutzername: string;
vorname: string;
nachname: string;
email: string;
emailbestaetigen: string;
neuesPasswort: string;
neuesPasswortbestaetigen: string;
straße: string;
hausnummer: string;
ort: string;
postleitzahl: number; 
telefon: number;
handy: number;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {
    benutzername: 'MM',
    vorname: 'Max',
    nachname: 'Mustermann',
    email: 'Max@Mustermann.de',
    emailbestaetigen: 'Max@Mustermann.de',
    neuesPasswort: 'maxmustermann',
    neuesPasswortbestaetigen: 'maxmustermann',
    straße: 'MaxMusterStraße',
    hausnummer: '1a',
    ort: 'Musterstadt',
    postleitzahl: 123456, 
    telefon: 11223344,
    handy: 55667788
  }];