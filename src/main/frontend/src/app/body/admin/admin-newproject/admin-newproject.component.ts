import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MatSort, MatTableDataSource, MatSelectChange, MatOption } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { CustomerService, Customer } from 'src/app/services/customer.service';  
import { UserService, User } from 'src/app/services/user.service';
import { Project, ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-admin-newproject',
  templateUrl: './admin-newproject.component.html',
  styleUrls: ['./admin-newproject.component.css']
})
export class AdminNewprojectComponent implements OnInit {
  projecttitle = new FormControl();
  projectmanager = new FormControl();
  deadline = new FormControl();
  description = new FormControl();
  budget = new FormControl();
  customer = new FormControl();

  customers: Customer[];
  users: User[];
  //selectedPerson: any;
  //selectedCustomer: Customer;



  hide = true;

  customerUser: User[];

    //TableSorting
    @ViewChild(MatSort) sort: MatSort;
    dataSource = new MatTableDataSource(ELEMENT_DATA);

  constructor(private customerService: CustomerService,
    private userService: UserService,
    private projectService: ProjectService){
    this.dataSource = new MatTableDataSource();
    

  }

  ngOnInit() {
   // this.getAllCustomers();
    this.getAllUsers();
    this.getOnlyCustomer();

    this.dataSource.data = ELEMENT_DATA;
    this.dataSource.sort = this.sort;
  }

 /*  getAllCustomers(){
    this.customerService.getAllCustomers()
    .subscribe(customers => this.customers = customers);
  } */

  getOnlyCustomer(){
    this.customerService.getCustomers()
    .subscribe(cus => this.customers = cus);
  }
 
  getAllUsers(){
    this.userService.getAllUsers()
    .subscribe (users => this.users = users);
  }
  
  newProject= new Project();

  setUser: User;
  setCustomer: any;
  selectedCustomer: any;

  addNewProject(){
    this.newProject.title = this.projecttitle.value;
    this.newProject.description = this.description.value;
    this.newProject.customer = this.setCustomer;
    this.newProject.projectmanager = this.setUser;
    this.newProject.budget = this.budget.value;
    this.newProject.enddate = this.deadline.value;
    //this.newProject.status.id = 1;

    console.log(this.newProject.status);
    console.log(this.newProject);

     this.projectService.addProject(this.newProject)
    .subscribe(
      result => console.log(result)
    ); 

     setTimeout(() => {
        window.location.reload();
      }, 10);
  }

 customerById(id: number){
    this.customerService.getCustomerById(id)
     .subscribe(cus => this.selectedCustomer = cus); 
  }

 columnsToDisplay = ['Projektname'];
  expandedElement: PeriodicElement | null;
  
}

export interface PeriodicElement{
projektname: string;

}


const ELEMENT_DATA: PeriodicElement[] = [
{
  projektname: 'Max'

}];