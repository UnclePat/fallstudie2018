import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminNewprojectComponent } from './admin-newproject.component';

describe('AdminNewprojectComponent', () => {
  let component: AdminNewprojectComponent;
  let fixture: ComponentFixture<AdminNewprojectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminNewprojectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminNewprojectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
