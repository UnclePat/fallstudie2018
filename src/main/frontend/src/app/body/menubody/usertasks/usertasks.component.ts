import { Component, ViewChild, OnInit,  } from '@angular/core';
import { MatTableDataSource,  MatDialog, MatDialogConfig, MatSort } from '@angular/material';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { ReallyDeleteComponent } from 'src/app/shared/popups/really-delete/really-delete.component';
import { ChangeTaskComponent } from '../changeTask/changeTask.component';
import { Router } from '@angular/router';
import { identifierModuleUrl } from '@angular/compiler';
import { UserService, User } from 'src/app/services/user.service';
import { TaskService } from 'src/app/services/task.service';


@Component({
  selector: 'app-usertasks', 
  templateUrl: './usertasks.component.html',
  styleUrls: ['./usertasks.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ])], 
  
})

export class UsertasksComponent implements OnInit {

     //TableSorting
  @ViewChild('sortTable1') sort: MatSort;
  @ViewChild('sortTable2') sort2: MatSort;
  dataSource: any;
  dataSource2: any;
  user: User; 

    constructor(private router: Router, 
      private dialog:MatDialog,
      private userService: UserService, 
      private taskService: TaskService){
      this.dataSource = new MatTableDataSource();
      this.dataSource2 = new MatTableDataSource();
      this.userService.getUser().subscribe(user => this.user = user);

    }

    ngOnInit() {        
      setTimeout(() => {        
        this.getAllResponsibleTasks();
        this.getAllSpectatedTasks();
      },500);

      setTimeout(() => {        
        this.dataSource.sort = this.sort;
        this.dataSource2.sort = this.sort2;

      },1000);
    }

    getAllResponsibleTasks() {
      //this.taskService.getAllUserTasks(this.userId)
      this.taskService.getAllUserTasks(this.user.id)
        .subscribe((data) => {this.dataSource = new MatTableDataSource(data);}
        )
    }

    getAllSpectatedTasks() {
      this.taskService.getAllSpectatedTasks()
        .subscribe((data) => {this.dataSource2 = new MatTableDataSource(data);}
        )
    }
    columnsToDisplay = ['projectId','id','title', 'status', 'deadline'];
    expandedElement: PeriodicElement | null;

     
    changeTask(projectid: number, taskid: number){
      this.router.navigate([{outlets: {primary: ['taskoverview', projectid, taskid], rightside: 'task'}}]);
    }

}

export interface PeriodicElement{
  ID: number;
  Titel: string;
  Status: string;
  Faelligkeit: Date;
  
  description: string;
  projectID: number;
}


const ELEMENT_DATA: PeriodicElement[] = [
  {
    ID: 2,
    projectID: 1,
    Titel: 'Logo austauschen',
    Status: 'Offen',
    Faelligkeit: new Date ('2018-12-24') ,
    description: `Logo Striche sind dünner als Icons.`
  }, {
    projectID: 1,
    ID: 3,
    Titel: 'Boxmodell',
    Status: 'In Arbeit',
    Faelligkeit:  new Date ('2018-2-24'),
    description: `Passt das Boxmodell? Oder doch mit Tabellen arbeiten?`
  },
  {
    projectID: 301,
    ID: 3,
    Titel: 'Aufbau',
    Status: 'In Arbeit',
    Faelligkeit:  new Date ('2018-12-24'),
    description: `Struktur der Anwendung überarbeiten und Aufgaben zuteilen`
  },
  { projectID: 2,
    ID: 203,
    Titel: 'Statistik/Farben',
    Status: 'In Arbeit',
    Faelligkeit:  new Date ('2018-12-24'),
    description: `Farben müssen noch angepasst werden`
  }];