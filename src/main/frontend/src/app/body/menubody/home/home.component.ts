import { Component,NgZone, OnInit, ViewChild } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatSort, MatTableDataSource, MatDialogConfig, MatDialog } from '@angular/material';
import { ReallyDeleteComponent } from 'src/app/shared/popups/really-delete/really-delete.component';
import { ChangeTaskComponent } from '../changeTask/changeTask.component';
import { Router } from '@angular/router';
import { TaskService, Task } from 'src/app/services/task.service';
import { UserService } from 'src/app/services/user.service';
import { routerNgProbeToken } from '@angular/router/src/router_module';
import { Subject } from 'rxjs';

//  TaskID, TaskTitel, Status, Fälligkeit, (verantwortlicher?), Überschrittene & rechtzeitig geschlossene Tasks

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})


export class HomeComponent implements OnInit {
  // Doughnut
  public doughnutChartData: number[]; //= [350, 450, 100];
  public doughnutChartData2: number[]; //= [this.closedTasksToday.length, 600];
  public doughnutChartType: string = 'doughnut';
  public doughnutChartLabels: string[] = ['Offen', 'In Arbeit', 'Geschlossen'];
  public doughnutChartLabels2: string[] = ['In Time', 'Überschritten'];
  // Doughnut events
  public chartClicked(e: any): void {
    console.log(e);
  }
  public chartHovered(e: any): void {
    console.log(e);
  }
  //Doughnut Colors
  public doughnutChartColors: any[] = [{ backgroundColor: ["#1c2364", "#a6a7a8", "#a4c73c"] }];

  


  //TableSorting
  @ViewChild(MatSort) sort: MatSort;
  //dataSource = new MatTableDataSource(ELEMENT_DATA);
  dataSource: any;

  ownTasks: Task[];

  openTasks: Task [] = [];
  closedTasks: Task [] = [];
  inWorkTasks: Task [] = [];

  openTasksToday: Task [] = [];
  closedTasksToday: Task [] = [];
  inWorkTasksToday: Task [] = [];

  constructor(private router: Router, 
    private dialog: MatDialog, 
    private taskService: TaskService,
    private userService: UserService) {
    this.dataSource = new MatTableDataSource();

  }

  ngOnInit() {
    this.getOwnTasksPerWeek();
    this.getClosedTasks(); 
    this.getInWorkTasks();
    this.getOpenTasks();
    this.getOpenTasksToday();
    this.getClosedTasksToday();
    this.getInWorkTasksToday();
    setTimeout(() => {
      return this.dataSource.sort = this.sort;
    },1000);
    
    
  }


  columnsToDisplay = ['projectID', 'ID', 'Titel', 'Status', 'Faelligkeit'];
  expandedElement: PeriodicElement | null;

  changeTask(projectid: number, taskid: number){
    this.router.navigate([{outlets: {primary: ['taskoverview', projectid, taskid], rightside: 'task'}}]);
    }

  getOwnTasksPerWeek() {
    this.taskService.getUserTasksThisWeek()
      .subscribe((data) => {this.dataSource = new MatTableDataSource(data);}
      )
  }

  getOpenTasks(): void {
    //this.taskService.getTasksByStatus(this.userId, 'offen')

    this.taskService.getAllUserTaksByStatus('offen')
      .subscribe( tasks => this.openTasks = tasks);
  }

  getClosedTasks(): void {
    //this.taskService.getTasksByStatus(this.userId, 'geschlossen')

    this.taskService.getAllUserTaksByStatus('geschlossen')
      .subscribe( tasks => this.closedTasks = tasks);
  }

  getInWorkTasks(): void {
    //this.taskService.getTasksByStatus(this.userId, 'in Arbeit')

    this.taskService.getAllUserTaksByStatus('in Arbeit')
      .subscribe( tasks => this.inWorkTasks = tasks);
  }

  getOpenTasksToday() {
    this.taskService.getAllUserTasksDeadline('offen')
      .subscribe( tasks => this.openTasksToday = tasks);
  }

  getClosedTasksToday() {
    this.taskService.getAllUserTasksDeadline('geschlossen')
      .subscribe( tasks => this.closedTasksToday = tasks);
  }

  getInWorkTasksToday() {
    this.taskService.getAllUserTasksDeadline('in Arbeit')
      .subscribe( tasks => this.inWorkTasksToday = tasks);

  }

}


export interface PeriodicElement {
  ID: number;
  Titel: string;
  Status: string;
  Faelligkeit: Date;

  description: string;
  projectID: number;
}


const ELEMENT_DATA: PeriodicElement[] = [
  {
    ID: 2,
    projectID: 1,
    Titel: 'Logo austauschen',
    Status: 'Offen',
    Faelligkeit: new Date('2018-12-24'),
    description: `Logo Striche sind dünner als Icons.`
  }, {
    projectID: 1,
    ID: 3,
    Titel: 'Boxmodell',
    Status: 'In Arbeit',
    Faelligkeit: new Date('2018-2-24'),
    description: `Passt das Boxmodell? Oder doch mit Tabellen arbeiten?`
  },
  {
    projectID: 301,
    ID: 3,
    Titel: 'Aufbau',
    Status: 'In Arbeit',
    Faelligkeit: new Date('2018-12-24'),
    description: `Struktur der Anwendung überarbeiten und Aufgaben zuteilen`
  },
  {
    projectID: 2,
    ID: 203,
    Titel: 'Statistik/Farben',
    Status: 'In Arbeit',
    Faelligkeit: new Date('2018-12-24'),
    description: `Farben müssen noch angepasst werden`
  }];


