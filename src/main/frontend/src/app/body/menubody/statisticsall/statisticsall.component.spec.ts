import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatisticsallComponent } from './statisticsall.component';

describe('StatisticsallComponent', () => {
  let component: StatisticsallComponent;
  let fixture: ComponentFixture<StatisticsallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatisticsallComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatisticsallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
