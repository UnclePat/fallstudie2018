import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { Color } from 'ng2-charts';
import { ProjectService } from 'src/app/services/project.service';

//ProjektTitel, ProjektId, Projektleiter, Status der Tasks, Anzahl offene Tasks, Anzahl offene Rechnungen, Anzahl Teammitglieder, Überschrittene & rechtzeitig geschlossene Tasks, Abgerechnete & noch nicht abgerechnete tasks

export interface PeriodicElement {
  Titel: string;
  Projektleiter: string;
  
}

const ELEMENT_DATA: PeriodicElement[] = [
  {Titel: 'Projekt 1', Projektleiter: 'Görich'},
  {Titel: 'Projekt 2', Projektleiter: 'Görich'},
  {Titel: 'Projekt 3', Projektleiter: 'Görich'},
];

@Component({
  selector: 'app-statisticsall',
  templateUrl: './statisticsall.component.html',
  styleUrls: ['./statisticsall.component.css']
})
export class StatisticsallComponent {
  // DOUGHNUT - Überschrittene Fähigkeit
      public doughnutChartData2:number[] = [600, 100];
      public doughnutChartType:string = 'doughnut';
      public doughnutChartLabels2:string[] = ['In Time', 'Überschritten'];
    // Doughnut/Balken events
    public chartClicked(e:any):void {
      console.log(e);
    }
    public chartHovered(e:any):void {
      console.log(e);
    }   
    //Doughnut Colors
    public doughnutChartColors:any[] = [{ backgroundColor: ["#1c2364", "#a6a7a8", "#a4c73c"] }];
     
    displayedColumns: string[] = ['select', 'Titel', 'Projektleiter'];
    dataSource: any; 

    constructor(private projectService: ProjectService) {
      this.dataSource = new MatTableDataSource();
    }

    ngOnInit () {
      this.getAllProjects();
    }

    getAllProjects() {
      this.projectService.getProjects()
        .subscribe((data) => {this.dataSource = new MatTableDataSource(data);})
    }

    //BARCHART - Fortschritt
    public barChartOptions:any = {
      scaleShowVerticalLines: false,
      responsive: true,
      tooltips:{
        mode: 'nearest'
      },
      scales:{
        xAxes: [{
          stacked: true,
        }],
        yAxes: [{
          stacked: true,
        }]
      }
    };
    public barChartLabels:string[] = ['Projekt 1', 'Projekt 2', 'Projekt 3'];
    public barChartType:string = 'bar';
    public barChartLegend:boolean = true;

    //Farben werden nicht richtig dargestellt!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    public barChartColor: any[] =  [{ backgroundColor: ["#1c2364", "#a6a7a8", "#a4c73c"] }];
    
    public barChartData:any[] = [
      {data: [100, 120, 85], label: 'Offen', backgroundColor: '#1c2364'},
      {data: [28, 48, 40], label: 'In Arbeit', backgroundColor: '#a6a7a8'},
      {data: [50, 12, 10], label: 'Geschlossen', backgroundColor:'#a4c73c'}
    ];
   
    public randomize():void {
      // Only Change 3 values
      let data = [
        Math.round(Math.random() * 100),
        59,
        80,
        (Math.random() * 100),
        56,
        (Math.random() * 100),
        40];
      let clone = JSON.parse(JSON.stringify(this.barChartData));
      clone[0].data = data;
      this.barChartData = clone;

    }


  //displayedColumns: string[] = ['select', 'Titel', 'Projektleiter'];
  //dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  selection = new SelectionModel<PeriodicElement>(true, []);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }


  // LINECHART - ABRECHNUNGEN
  // lineChart
  public lineChartData:Array<any> = [
    {data: [650, 300, 100, 5000, 800, 300, 5000], label: 'Geschlossen, Noch nicht berechnet'},
    {data: [2000, 500, 3000, 200, 1000, 1500, 0], label: 'Geschlossen, Abgerechnet'}
  ];
  public lineChartLabels:Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  public lineChartOptions:any = {
    responsive: true
  };
  public lineChartColors:Array<any> = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public lineChartLegend:boolean = true;
  public lineChartType:string = 'line';
 
  
}
