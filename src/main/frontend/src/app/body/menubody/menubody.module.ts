import { NgModule }             from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersetupsComponent } from './usersetups/usersetups.component';
import { UsertasksComponent } from './usertasks/usertasks.component';
import { StatisticsallComponent } from './statisticsall/statisticsall.component';
import { HomeComponent } from './home/home.component';

import { MatTableModule, MatSortModule, MatCheckboxModule, MatChipsModule, MatIconModule, MatFormFieldModule, MatSelectModule, MatInputModule, MatDialog, MatDialogModule, MatDatepicker, MatDatepickerModule } from '@angular/material';
import { ChartsModule } from 'ng2-charts';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TaskService } from 'src/app/services/task.service';
import { ChangeTaskComponent } from './changeTask/changeTask.component';
import { RouterModule } from '@angular/router';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { CoreModule } from 'src/app/core/core.module';

@NgModule({

    entryComponents: [ 
      ChangeTaskComponent
    ],

    declarations: [
        UsersetupsComponent,
        UsertasksComponent,
        StatisticsallComponent,
        HomeComponent,
        ChangeTaskComponent
        
      ],

      imports: [
          MatTableModule,
          CommonModule,
          ChartsModule,
          MatSortModule,
          MatCheckboxModule,
         MatChipsModule,
         MatIconModule,
         FormsModule,
         ReactiveFormsModule,
         MatFormFieldModule,
         MatSelectModule,
         MatInputModule,
         MatDialogModule,
         MatDatepickerModule,
         RouterModule,
         ScrollingModule,
         CoreModule
         
      ],

      exports: [
        HomeComponent,
        StatisticsallComponent,
        UsersetupsComponent,
        UsertasksComponent,
        ChangeTaskComponent
    
      ],

      providers: [
        TaskService
      ]
})
export class MenubodyModule {}