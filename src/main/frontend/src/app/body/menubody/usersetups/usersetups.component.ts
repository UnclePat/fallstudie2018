import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { MatSort, MatTableDataSource, MatTab } from '@angular/material';
import { UserService, User } from 'src/app/services/user.service';

@Component({
  selector: 'app-usersetups',
  templateUrl: './usersetups.component.html',
  styleUrls: ['./usersetups.component.css'],
  providers:  [UserService]
})
export class UsersetupsComponent implements OnInit {
  email = new FormControl('', [Validators.required, Validators.email]);
  emailbestaetigen = new FormControl('', [Validators.required, Validators.email]);
  vorname = new FormControl('', [Validators.required]); 
  nachname = new FormControl('', [Validators.required]);   
  neuesPasswort = new FormControl('', [Validators.required]); 
  neuesPasswortbestaetigen = new FormControl('', [Validators.required]); 
  telefon = new FormControl('', [Validators.required]); 
  handy = new FormControl('', [Validators.required]); 


  getErrorMessageVorname() {
    return this.vorname.hasError('required') ? 'Bitte geben Sie Ihren Vornamen ein' :
      this.vorname.hasError('vorname') ? 'Bitte geben Sie Ihren Vornamen ein' :
    '';}

  getErrorMessageNachname() {
    return this.nachname.hasError('required') ? 'Bitte geben Sie Ihren Nachnamen ein' :
      this.nachname.hasError('nachname') ? 'Bitte geben Sie Ihren Nachnamen ein' :
    '';}

  getErrorMessageEMail() {
    return this.email.hasError('required') ? 'Bitte geben Sie eine gültige E-Mail Adresse ein' :
      this.email.hasError('email') ? 'Bitte geben Sie eine gültige E-Mail Adresse ein' :
    '';}

  getErrorMessageEMailBestaetigen() {
    return this.emailbestaetigen.hasError('required') ? 'Bitte bestätigen Sie Ihre E-Mail Adresse' :
      this.emailbestaetigen.hasError('emailbestaetigen') ? 'Bitte bestätigen Sie Ihre E-Mail Adresse' :
    '';}

  getErrorMessageNeuesPasswort() {
    return this.neuesPasswort.hasError('required') ? 'Bitte geben Sie ein neues Passwort ein' :
      this.neuesPasswort.hasError('neuesPasswort') ? 'Bitte geben Sie ein neues Passwort ein' :
    '';}

  getErrorMessageNeuesPasswortBestaetigen() {
    return this.neuesPasswortbestaetigen.hasError('required') ? 'Bitte bestätigen Sie Ihr neues Passwort' :
      this.neuesPasswortbestaetigen.hasError('neuesPasswortbestaetigen') ? 'Bitte bestätigen Sie Ihr neues Passwort' :
    ''; }

  getErrorMessageTelefon() {
    return this.telefon.hasError('required') ? 'Bitte geben Sie Ihre Telefonnummer ein' :
      this.telefon.hasError('nachname') ? 'Bitte geben Sie Ihre Telefonnummer ein' :
    '';}

  getErrorMessageHandy() {
    return this.handy.hasError('required') ? 'Bitte geben Sie Ihre Handynummer ein' :
      this.handy.hasError('nachname') ? 'Bitte geben Sie Ihre Handynummer ein' :
    '';}

  hide = true;

 
  @Input()user: User;  

  
  constructor(private userService: UserService,
    private formBuilder: FormBuilder,

  ){ 

  }

  ngOnInit() {
    this.getUser();
    //this.service.getRole();
  }

  saveUser() {
    //this.myForm.get('housenumber').valueChanges.subscribe(val => {
    //  alert(val)
    //});

    this.userService.updateUser(this.user)
      .subscribe(); //() => this.refreshPage());
  }

  getUser(): void {
    this.userService.getUser()
      .subscribe(user => this.user = user);
  }

  refreshPage() {
    location.reload();
  }
}
