import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersetupsComponent } from './usersetups.component';

describe('UsersetupsComponent', () => {
  let component: UsersetupsComponent;
  let fixture: ComponentFixture<UsersetupsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersetupsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersetupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
