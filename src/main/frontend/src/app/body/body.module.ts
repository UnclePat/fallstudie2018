import { NgModule, CUSTOM_ELEMENTS_SCHEMA }             from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { MenubodyModule } from './menubody/menubody.module';
import { ProjectbodyModule } from './projectbody/projectbody.module';
import { TaskbodyModule } from './taskbody/taskbody.module';
import { MenubodyComponent } from './menubody/menubody.component';
import { ProjectbodyComponent } from './projectbody/projectbody.component';
import { TaskbodyComponent } from './taskbody/taskbody.component';

import { MatCardModule, MatToolbarModule, MatSidenavModule, MatListModule, MatButtonModule, MatIconModule, MatCheckbox, MatTableModule, MatPaginator, MatPaginatorModule, MatCheckboxModule, MatSnackBarModule, MatMenuModule, MatFormFieldModule, MatInputModule, MatDatepickerModule, MatSortModule } from '@angular/material';
import { AppRoutingModule } from '../app-routing.module';
import { AdminModule } from './admin/admin.module';
import { FormsModule } from '@angular/forms';


@NgModule({
    declarations: [
        MenubodyComponent,
        TaskbodyComponent,
        ProjectbodyComponent,
      ],

      imports: [
        BrowserModule, 
        BrowserAnimationsModule,
        AppRoutingModule, 	

        MenubodyModule,
        ProjectbodyModule,
        TaskbodyModule,
        AdminModule,
        MatTableModule,
        MatSortModule,
        MatCardModule,
        MatToolbarModule,
        MatSidenavModule,
        MatListModule,
        MatButtonModule,
        MatIconModule,
        MatCheckboxModule,
        MatPaginatorModule,
        MatSnackBarModule,
        MatMenuModule,
        MatFormFieldModule,
        MatInputModule,
        FormsModule,
        MatDatepickerModule,
        MatCheckboxModule,
        
      ],

      exports: [
        MenubodyComponent,
        TaskbodyComponent,
        ProjectbodyComponent,
      
      ],
})
export class BodyModule {}