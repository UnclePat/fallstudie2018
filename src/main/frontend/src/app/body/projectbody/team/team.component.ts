import { Component, OnInit } from '@angular/core';
import { Person, PersonService, Member } from 'src/app/services/person.service';
import { queryRefresh } from '@angular/core/src/render3/query';
import { element } from '@angular/core/src/render3/instructions';
import { RightsideComponent } from 'src/app/core/rightside/rightside.component';
import { ActivatedRoute } from '@angular/router';
import { UserService, User } from 'src/app/services/user.service';
import { ProjectService } from 'src/app/services/project.service';
import { MatDialog } from '@angular/material';
import { ReallyDeleteComponent } from 'src/app/shared/popups/really-delete/really-delete.component';
import { FormControl } from '@angular/forms';


@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css'],
  providers: [PersonService]
})

export class TeamComponent implements OnInit {
 
  members: Person[];
  selectedPerson: any;
  
  currentRole: String;
  users: User[];
  projectManagerId: Number;
  currentUserId: Number;

  newUser = new FormControl();

  deleteMember = new Member;

  constructor(private personService: PersonService,
    private route: ActivatedRoute,
    private userService: UserService,
    private projectService: ProjectService,
    public dialog: MatDialog) {
    //getPeopleById ändern!!!!!!!!!!!!!!!!!!!!!!!!!
    

    //UserRole (Admin or PM?)
    this.userService.getUser().subscribe(user => this.currentRole = user.roles[0].name);
    this.userService.getUser().subscribe(user => this.currentUserId = user.id); 

    const projectId = +this.route.snapshot.paramMap.get('projectid');
    this.projectService.getProjectById(projectId).subscribe(user => this.projectManagerId = user.projectmanager.id );

  }

  ngOnInit() {
    this.getAllTeamMembers();
    this.getAllUsers();
  }

  onPersonSelected(event){
    this.selectedPerson=<Person>event.target.people[0];
    console.log();
  }

  newMember = new Person();
  setUser: Person;
  addMember(){
    console.log(this.setUser);

    const projectId = +this.route.snapshot.paramMap.get('projectid');
    this.personService.addProjectMember(projectId, this.setUser)
      .subscribe();
    
      setTimeout(() => {
        this.getAllTeamMembers();
      }, 100);
    
  }
 
  getAllTeamMembers () {
    const id = +this.route.snapshot.paramMap.get('projectid');
    this.personService.getAllProjectMembers(id)
      .subscribe(members => this.members = members);
  }

  getAllUsers() {
    this.userService.getAllUsers()
      .subscribe (users => this.users = users);
  }

  openReallyDelete(member: User){
    const projectId = +this.route.snapshot.paramMap.get('projectid');


    this.deleteMember.id = member.id;
    this.deleteMember.projectId = projectId;

    console.log(member);
    
    this.dialog.open(ReallyDeleteComponent, {data: { team: this.deleteMember }});

  }

  


get isAdminOrPM(){

 // console.log(this.projectManagerId + 'PMId');
  //console.log(this.currentUserId + 'userId');

  if( this.currentRole === 'ADMIN' || this.currentUserId === this.projectManagerId ){
    //console.log('Is Admin or PM'); 
    return true;
  }else{
    //console.log('Isnt Admin or PM');
    return false;
  }
  
}

}
