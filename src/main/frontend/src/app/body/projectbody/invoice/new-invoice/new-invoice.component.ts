import {Component} from '@angular/core';
import {MatTableDataSource, MatSort} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import { MatMenuModule} from '@angular/material/menu';
import { ViewChild } from '@angular/core';

export interface PeriodicElement {
  IDTask: Number;
  AufgabeTask: string;
  //Abrechnen: number;
  BetragTask: Number;}
  
  const ELEMENT_DATA: PeriodicElement[] = [  
  {IDTask: 1, AufgabeTask: 'Task 1 beispielsweise',  BetragTask:  12},
  {IDTask: 2, AufgabeTask: 'Ein weiterer Tasks',    BetragTask: 15},
  {IDTask: 3, AufgabeTask: 'Passt die Seite so?',   BetragTask: 111},
  ]
/**
 * @title Basic use of `<table mat-table>`
 */
@Component({
  selector: 'app-new-invoice',
  styleUrls: ['new-invoice.component.css'],
  templateUrl: 'new-invoice.component.html',
})
export class NewInvoiceComponent {
  displayedColumns: string[] = ['IDTask', 'AufgabeTask', 'Auswählen', 'BetragTask'];
  //dataSource = ELEMENT_DATA;
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  selection = new SelectionModel<PeriodicElement>(true, []);
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.dataSource.sort = this.sort;
  }

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
      const numSelected = this.selection.selected.length;
      const numRows = this.dataSource.data.length;
      return numSelected === numRows;
    }
  
    /** Selects all rows if they are not all selected; otherwise clear selection. */
    masterToggle() {
      this.isAllSelected() ?
          this.selection.clear() :
          this.dataSource.data.forEach(row => this.selection.select(row));
    }
}
