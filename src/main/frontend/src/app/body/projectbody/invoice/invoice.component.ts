import {Component, Inject} from '@angular/core';
import {MatTableDataSource, MatSort, MatDialogConfig, MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { AdditionalCostsComponent } from 'src/app/shared/popups/additional-costs/additional-costs.component';
import { Invoice, InvoiceService, OtherExpanses } from 'src/app/services/invoice.service';
import { ActivatedRoute } from '@angular/router';
import { ViewChild } from '@angular/core';
import { Project, ProjectService } from 'src/app/services/project.service';
import { FormBuilder, FormGroup, FormArray, FormControl } from '@angular/forms';

export interface PeriodicElement {
  ID: Number;
  Aufgabe: string;
  description: string;
  //Abrechnen: number;
  Betrag: Number;}
  
  const ELEMENT_DATA: PeriodicElement[] = [  
  {ID: 1, Aufgabe: 'Login anpassen', description:'Beschreibung',  Betrag:  144},
  {ID: 2, Aufgabe: 'NewTask vervollständigen', description:'Beschreibung',    Betrag: 200},
  {ID: 3, Aufgabe: 'ForgotPW Email senden', description:'Beschreibung',    Betrag: 111},
  {ID: 4, Aufgabe: 'Http Client', description:'Beschreibung',  Betrag: 222},
];

/**
 * @title Basic use of `<table mat-table>`
 */
@Component({
  selector: 'app-invoice',
  styleUrls: ['invoice.component.css'],
  templateUrl: 'invoice.component.html',
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ])], 
})
export class InvoiceComponent {
  columnsToDisplay: string[] = ['Abrechnen', 'ID', 'Beschreibung', 'Aufgabe', 'Betrag'];
  //dataSource = ELEMENT_DATA;
  dataSource: any;
  selection = new SelectionModel<PeriodicElement>(true, []);
  //dataSource: any;
  @ViewChild(MatSort) sort: MatSort;

  project: Project;
  billPosts: any; 
  options = new Array();
  arrayList= {otherExpanses: []};
  checked: boolean;


  constructor(private dialog: MatDialog, 
    private invoiceService: InvoiceService,
    private route: ActivatedRoute,
    private projectService: ProjectService,
    private snackBar: MatSnackBar) {
      this.dataSource = new MatTableDataSource;
      const id = +this.route.snapshot.paramMap.get('projectid');
      this.projectService.getProjectById(id)
        .subscribe(project => this.project = project);
  }

  ngOnInit() {
    this.getAllBillPosts();
    this.dataSource.sort = this.sort;
    this.fillCheckboxes();
  }

  fillCheckboxes() {
    this.getAllBillPosts();
    setTimeout(() => {
      var a = new Array(this.dataSource.data.length);
      console.log(this.dataSource.data.length);
          for (var i=0; i < this.dataSource.data.length; i++) {
            this.options[i]= this.dataSource.data[i].id;
      } 
      console.log(this.options);
    }, 1000);
  }

  newInvoice = new OtherExpanses();

  onCheckboxChange(option, event) {
    this.checked = !this.checked
    if(this.checked) {
      this.newInvoice.id = option.id;
      this.arrayList.otherExpanses.push(this.newInvoice);
    } else {
      for(var i=0 ; i < this.arrayList.otherExpanses.length; i++) {
        if(this.arrayList.otherExpanses[i].id == option.id){
          this.arrayList.otherExpanses.splice(i,1);
        }
      }
    }
  }

  createInvoice () {
    this.invoiceService.getInvoices(this.arrayList).subscribe();
    this.snackBar.open("Rechnung wurde erstellt. Sie können die Rechnung unter den Projektanhängen aufrufen.", "", {
      duration: 2000,
    });
    this.getAllBillPosts();
  }

  getAllBillPosts () {
    const id = +this.route.snapshot.paramMap.get('projectid');
    this.invoiceService.getBillPostsByProjectID (id)
      .subscribe((data) => {this.dataSource = new MatTableDataSource(data);}
      );
  }

  addCosts(){    
    this.dialog.open(AdditionalCostsComponent, {data: {project: this.project}});
  }

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
      const numSelected = this.selection.selected.length;
      const numRows = this.dataSource.data.length;
      return numSelected === numRows;
    }
  
    /** Selects all rows if they are not all selected; otherwise clear selection. */
    masterToggle() {
      this.isAllSelected() ?
          this.selection.clear() :
          this.dataSource.data.forEach(row => this.selection.select(row));
    }
  }

