import { NgModule }             from '@angular/core';

import { OverviewComponent } from './overview/overview.component';
import { NewtaskComponent } from './newtask/newtask.component';
import { StatisticsprojectComponent } from './statisticsproject/statisticsproject.component';
import { TeamComponent } from './team/team.component';
import { AttachmentComponent } from './attachment/attachment.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { NewInvoiceComponent } from './invoice/new-invoice/new-invoice.component';
import { MatTableModule, MatSortModule, MatIconModule, MatCardModule, MatFormField, MatFormFieldModule, MatOptionModule, MatSelectModule, MatMenuModule, MatCheckboxModule, MatDialogModule, MatDatepickerModule, MatInputModule, MatSnackBarModule } from '@angular/material';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { RouterModule } from '@angular/router';
import { PersonService } from 'src/app/services/person.service';
import { BookingTimeComponent } from 'src/app/shared/popups/booking-time/booking-time.component';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import { AdditionalCostsComponent } from 'src/app/shared/popups/additional-costs/additional-costs.component';

@NgModule({

    declarations: [
        OverviewComponent,
        NewtaskComponent,
        NewInvoiceComponent,
        StatisticsprojectComponent,
        TeamComponent,
        AttachmentComponent,
        InvoiceComponent,
       
      ],

      imports: [
          MatTableModule,
          CommonModule,
          MatSortModule,
         MatIconModule,
         FormsModule,
         ReactiveFormsModule,
         ChartsModule,
         DragDropModule,
         MatCardModule,
         MatFormFieldModule,
         MatOptionModule,
         MatSelectModule,
         MatMenuModule,
         MatCheckboxModule,
         RouterModule,
         BrowserModule,
         BrowserAnimationsModule,
         NoopAnimationsModule,
         MatDatepickerModule,
         MatInputModule,
         MatSnackBarModule
         
      ],

      exports: [
        OverviewComponent,
        NewtaskComponent,
        NewInvoiceComponent,
        StatisticsprojectComponent,
        TeamComponent,
        AttachmentComponent,
        InvoiceComponent,
        
      ],

      providers: [
        PersonService
      ],

      entryComponents: [ 
        BookingTimeComponent,
        AdditionalCostsComponent,
        NewtaskComponent
      ],
})
export class ProjectbodyModule {}