import { Component, OnInit } from '@angular/core';

import { MatDialogConfig, MatDialog } from '@angular/material';
import { ReallyDeleteComponent } from 'src/app/shared/popups/really-delete/really-delete.component';
import { File, AttachmentService } from 'src/app/services/attachement.service';
import { AdminModule } from '../../admin/admin.module';
import { UserService } from 'src/app/services/user.service';
import { ActivatedRoute } from '@angular/router';
import { ProjectService, Project } from 'src/app/services/project.service';


@Component({
  selector: 'app-attachment',
  templateUrl: './attachment.component.html',
  styleUrls: ['./attachment.component.css']
})
export class AttachmentComponent implements OnInit {
  selectedFile: any ;
  files: File[];
  
   
  currentRole: String;
  currentUserId: Number;
  

  constructor(private dialog: MatDialog, 
    private userService: UserService,
    private fileService: AttachmentService,
    private projectServices: ProjectService,
    private route: ActivatedRoute,) { 
    //this.files = service.getAllFiles();

   this.getAttachment();

   //const projectId = +this.route.snapshot.paramMap.get('projectid');
  // console.log(this.projectServices.getProjectById(projectId).subscribe(project => this.x = project));

    //UserRole (Admin or PM?)
    this.userService.getUser().subscribe(user => this.currentRole = user.roles[0].name);
    this.userService.getUser().subscribe(user => this.currentUserId = user.id); 
   }
 
    x: Project;
    fileToUpload: any;
  // File auswählen und Daten übergeben 
  onFileSelected(fileList: FileList){
    this.fileToUpload = fileList.item(0);
   }

   currentFile:any;
  downloadAttachment(file: any){
    console.log('download file' + file.id);
    const projectId = +this.route.snapshot.paramMap.get('projectid');
    this.fileService.downloadProjectFile(projectId, file.id)
    /*  .subscribe(data => {
      file => console.log(file)}, 
    error => console.log(error) ) */; 
  }

  deleteAttachement(){
    const dialogConfig = new MatDialogConfig();
    this.dialog.open(ReallyDeleteComponent,dialogConfig);
   }

   getAttachment(){
      const projectId = +this.route.snapshot.paramMap.get('projectid');
      this.fileService.getFilesByProjectId(projectId)
        .subscribe(files => this.files = files);
   } 

   addFile: File;


   addAttachment(){
     //console.log(this.fileToUpload);
    const projectId = +this.route.snapshot.paramMap.get('projectid');

      this.fileService.addFile(this.fileToUpload, projectId )
      .subscribe(data => {
        result => console.log(result)
      }, error => { console.log(error)});
    this.getAttachment();  
   }
  

   ngOnInit() {     
  }
   
  get isAdmin(){
    //console.log(this.currentRole + 'currentUserRole');
    //console.log(this.currentUserId + 'currentUserId');
  
    if( this.currentRole === 'ADMIN'){
       return true;
    }else{
      return false; 
    }
 }


  
  
}
