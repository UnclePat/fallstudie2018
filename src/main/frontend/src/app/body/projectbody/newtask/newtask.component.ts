import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { PersonService, Person } from 'src/app/services/person.service';
import { ActivatedRoute } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Task, TaskService, Priority, Status } from 'src/app/services/task.service';
import { User, UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-newtask',
  templateUrl: './newtask.component.html',
  styleUrls: ['./newtask.component.css']
})
export class NewtaskComponent implements OnInit {
  title = new FormControl('', [Validators.required]);
  responsible = new FormControl('', [Validators.required]);
  project = new FormControl('');
  deadline = new FormControl('', [Validators.required]);
  priority = new FormControl('', [Validators.required]);
  status = new FormControl('', [Validators.required]);
  description = new FormControl('', [Validators.required]);
  expectedExpanse = new FormControl('');
  //priority = new FormControl('', [Validators.required]);
  //priority = new FormControl('', [Validators.required]);
  
  members: Person[];
  priorities: Priority[];
  taskStatus: Status[];
  newTask = new Task();
  setMember: any;
  setStatus: any;
  setPriority: any;

  constructor(private personService: PersonService,
    private taskService: TaskService,
    public dialogRef: MatDialogRef<NewtaskComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { 
    }

  ngOnInit() {
    this.getAllTeamMembers();
    this.getPriorities();
    this.getStatus();
  }

  getPriorities() {
    this.taskService.getPriorities()
      .subscribe(priorities => this.priorities = priorities);
  }

  getStatus() {
    this.taskService.getStatus()
      .subscribe(tasksStatus => this.taskStatus = tasksStatus);
  }

  getAllTeamMembers () {
    this.personService.getAllProjectMembers(this.data.project.id)
      .subscribe(members => this.members = members);
  }

  addTask() {

    this.newTask.title = this.title.value;
    this.newTask.responsible = this.setMember;
    this.newTask.projectId = this.data.project.id;
    this.newTask.deadline = this.deadline.value;
    this.newTask.priority = this.setPriority;
    this.newTask.status = this.setStatus;
    this.newTask.description = this.description.value;
    this.newTask.expectedExpense = this.expectedExpanse.value;

    console.log(this.newTask );
    
    this.taskService.addTask(this.newTask, this.data.project.id)
    .subscribe(
      result => console.log(result)
    );

    setTimeout(() => {
      window.location.reload();
    }, 10);
  }
}
