import { Component, OnInit } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { TaskService, Task } from 'src/app/services/task.service';
import { ActivatedRoute } from '@angular/router';
import { ProjectService, Project } from 'src/app/services/project.service';
import { PersonService, Person } from 'src/app/services/person.service';

// Status, Fälligkeiten, Tasks, 

@Component({
  selector: 'app-statisticsproject',
  templateUrl: './statisticsproject.component.html',
  styleUrls: ['./statisticsproject.component.css']
})
export class StatisticsprojectComponent implements OnInit {

  // DOUGHNUT Diagramm
  public doughnutChartType: string = 'doughnut';
  public doughnutChartLabels: string[] = ['Offen', 'In Arbeit', 'Geschlossen'];
  public doughnutChartLabels2: string[] = ['In Time', 'Überschritten'];

  openTasks: Task [] = [];
  closedTasks: Task [] = [];
  inWorkTasks: Task [] = [];

  openTasksToday: Task [] = [];
  closedTasksToday: Task [] = [];
  inWorkTasksToday: Task [] = [];

  members: Person[];
  project: Project;  

  projectid = +this.route.snapshot.paramMap.get('projectid');

  // Doughnut/Balken events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }
  //Doughnut Colors
  public doughnutChartColors: any[] = [{ backgroundColor: ["#1c2364", "#a6a7a8", "#a4c73c"] }];

  constructor(private taskService: TaskService,
    private route: ActivatedRoute,
    private personService: PersonService,
    private projectService: ProjectService) {  
  }

  tasks: Task[];

  ngOnInit() {
    this.getProjectById();
    this.getOpenTasks();
    this.getClosedTasks();
    this.getInWorkTasks();
    this.getOpenTasksToday();
    this.getInWorkTasksToday();
    this.getClosedTasksToday();
    this.getProjectMembers();
  }

  getProjectById () {
    this.projectService.getProjectById(this.projectid)
      .subscribe(project => this.project = project);
  }

  getOpenTasks(): void {
    this.taskService.getTasksByStatus(this.projectid, 'offen')
      .subscribe( tasks => this.openTasks = tasks);
  }

  getClosedTasks(): void {
    this.taskService.getTasksByStatus(this.projectid, 'geschlossen')
      .subscribe( tasks => this.closedTasks = tasks);
  }

  getInWorkTasks(): void {
    this.taskService.getTasksByStatus(this.projectid, 'in Arbeit')
      .subscribe( tasks => this.inWorkTasks = tasks);
  }

  getOpenTasksToday() {
    this.taskService.getTaskByStatusDeadline(this.projectid, 'offen', 0)
      .subscribe(tasks => this.openTasksToday = tasks);
  }

  getInWorkTasksToday() {
    this.taskService.getTaskByStatusDeadline(this.projectid, 'in Arbeit', 0)
      .subscribe(tasks => this.inWorkTasksToday = tasks);
  }

  getClosedTasksToday() {
    this.taskService.getTaskByStatusDeadline(this.projectid, 'geschlossen', 0)
      .subscribe(tasks => this.closedTasksToday = tasks);
  }

  getProjectMembers() {
    this.personService.getAllProjectMembers(this.projectid)
      .subscribe(members => this.members = members);
  }


  //Kanban-Board Drag&Drop
  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }
}
