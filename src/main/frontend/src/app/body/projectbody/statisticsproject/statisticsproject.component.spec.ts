import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatisticsprojectComponent } from './statisticsproject.component';

describe('StatisticsprojectComponent', () => {
  let component: StatisticsprojectComponent;
  let fixture: ComponentFixture<StatisticsprojectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatisticsprojectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatisticsprojectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
