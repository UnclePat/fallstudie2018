import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatDialogConfig, MatDialog } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { ReallyDeleteComponent } from 'src/app/shared/popups/really-delete/really-delete.component';
import { ChangeTaskComponent } from '../../menubody/changeTask/changeTask.component';
import { trigger, state, transition, style, animate } from '@angular/animations';
import { TaskoverviewComponent } from '../../taskbody/taskoverview/taskoverview.component';
import { BookingTimeComponent } from 'src/app/shared/popups/booking-time/booking-time.component';
import { Project, ProjectService } from 'src/app/services/project.service';
import { CloseProjectComponent } from 'src/app/shared/popups/close-project/close-project.component';
import { TaskService, Task } from 'src/app/services/task.service';
import { NewtaskComponent } from '../newtask/newtask.component';


// Task ID, Titel, Task-Verantwortliche, Status, Fälligkeit + ProjektID, Projektleiter, Projektbeschreibung

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css'],
  providers: [ProjectService],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ])], 
})
export class OverviewComponent implements OnInit {  
  
  @ViewChild(MatSort) sort: MatSort;

  project: Project; 
  dataSource: any;
  task: Task; 

  constructor(
    private router: Router, 
    private dialog:MatDialog,
    private projectService: ProjectService,
    private taskService: TaskService,
    private route: ActivatedRoute){
      this.router.routeReuseStrategy.shouldReuseRoute = () => false;
      this.dataSource = new MatTableDataSource();
  }

  ngOnInit() {
    /*this.route.params.subscribe(
      params => {
         let id= +params['projectid'];
         this.getProjectById();
         this.getAllTasksByProject();
         this.dataSource.sort = this.sort;
      });*/

      this.getProjectById();
      this.getAllTasksByProject();
    
    
      //this.dataSource.sort = this.sort;
  }
 
  columnsToDisplay= ['id', 'title', 'responsible', 'status','deadline'];
  expandedElement: PeriodicElement|null;

  refresh(): void {
    window.location.reload();
  }

  newTask(){
    const dialogConfig = new MatDialogConfig();
    this.dialog.open(NewtaskComponent,  {data: {project: this.project}});
  }

  closeProject(){
    this.dialog.open(CloseProjectComponent, {data: {project: this.project}});
  }

  changeTask(taskid: number){
    const projectid = +this.route.snapshot.paramMap.get('projectid');
    this.router.navigate([{outlets: {primary: ['taskoverview', projectid, taskid], rightside: 'task'}}]);
    }
  
   bookingTime (projectid: number, taskid: number) {
    this.taskService.getTaskById(projectid,taskid)
      .subscribe(task => this.task = task);
    setTimeout(() => {
      this.dialog.open(BookingTimeComponent,{data: {task: this.task}});
    },1000);
    //this.dialog.open(BookingTimeComponent,{data: {task: this.task}});
   }

   getProjectById (): void {
    const id = +this.route.snapshot.paramMap.get('projectid');
    this.projectService.getProjectById(id)
      .subscribe(project => this.project = project);
      setTimeout(() => {
        return this.dataSource.sort = this.sort;
      },1000);
   }

   getAllTasksByProject (): void {
    const id = +this.route.snapshot.paramMap.get('projectid');
    this.taskService.getTasks(id)
      .subscribe((data) => {this.dataSource = new MatTableDataSource(data);
        this.dataSource.sort = this.sort;
      }
      )

      
   }

   
}


export interface PeriodicElement {
  name: string;
  ID: number;
  verantwortlich: string;
  status: string;
  deadline: Date;
  description: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {ID: 1, name: 'Task 1', verantwortlich: 'Luisa Görich', status: 'In Arbeit', deadline: new Date('2018-12-23'),description: 'Beschreibung'},
  {ID: 2, name: 'Task 2', verantwortlich: 'Lisa Bandholtz', status: 'Geschlossen', deadline:  new Date('2018-12-23'),description: 'Beschreibung'},
  {ID: 3, name: 'Task 3', verantwortlich: 'Luisa Görich', status: 'Geschlossen', deadline:  new Date('2018-12-23'), description: 'Beschreibung'},
  {ID: 4, name: 'Task 4', verantwortlich: 'Lisa Bandholtz', status: 'Offen', deadline:  new Date('2018-12-23'),description: 'Beschreibung'},
 
];
