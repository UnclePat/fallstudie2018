import { Component, OnInit, Pipe, PipeTransform, Input} from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Task, TaskService, Status, Priority } from 'src/app/services/task.service';
import { log } from 'util';
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Project, ProjectService } from 'src/app/services/project.service';
import { Person, PersonService } from 'src/app/services/person.service';
import { FormControl, Validators } from '@angular/forms';
import { CloseTaskComponent } from 'src/app/shared/popups/close-task/close-task.component';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { NewMailComponent } from 'src/app/shared/popups/newmail/new-mail.component';
import { CustomerMailComponent } from '../customerMail/customermail.component';
import { User, UserService } from 'src/app/services/user.service';

@Pipe({
  name: 'filterUnique',
  pure: false
})

@Component({
  selector: 'app-taskoverview',
  templateUrl: './taskoverview.component.html',
  styleUrls: ['./taskoverview.component.css'],
  providers: [TaskService]
})

export class TaskoverviewComponent implements OnInit, PipeTransform {
  task: Task;
  project: Project; 
  tasks: Task []; 
  members: Person[];
  taskStatus: Status[];
  priorities: Priority[];
  ownUser: User;
  setStatus: any;
  setPriority: any;
  setResponsible: any;


  transform(value: any, args?: any): any {

    // Remove the duplicate elements
    let uniqueArray = value.filter(function (el, index, array) { 
      return array.indexOf (el) == index;
    });

    return uniqueArray;
  }
  status = new FormControl('');
  priority = new FormControl('');
  responsible = new FormControl();


  constructor(
    private taskService: TaskService,
    private route: ActivatedRoute,
    private projectService: ProjectService,
    private personService: PersonService,
    private dialog:MatDialog,
    private userService: UserService
    ) {
      this.userService.getUser().subscribe(user => this.ownUser = user);
      this.getTaskById();
  }
  toggle:any;
  spectator: boolean;
  selectedPriority: any;

  ngOnInit(): void {
    this.getTaskById();
    this.getProjectByProjectId();
    this.getAllTeamMembers();
    this.getStatus();
    this.getPriorities();
    setTimeout(() => {
      this.checkIfUserIsSpec();
      this.setPriority = this.task.priority.id;
      this.setStatus = this.task.status.id;
      this.setResponsible = this.task.responsible.id;
    },1000);
  }

  getStatus() {
    this.taskService.getStatus()
      .subscribe(tasksStatus => this.taskStatus = tasksStatus);
  }

  getPriorities() {
    this.taskService.getPriorities()
      .subscribe(priorities => this.priorities = priorities);
  }
 
  getTaskById(): void {
    const projectid = +this.route.snapshot.paramMap.get('projectid');
    const taskid = +this.route.snapshot.paramMap.get('taskid');
    this.taskService.getTaskById(projectid, taskid)
      .subscribe(task => this.task = task);
  }

  getProjectByProjectId(): void {
    const projectid = +this.route.snapshot.paramMap.get('projectid');
    this.getTaskById();
    this.projectService.getProjectById(projectid)
      .subscribe (project => this.project = project);
  }

  getAllTeamMembers () {
    const projectid = +this.route.snapshot.paramMap.get('projectid');
    this.personService.getAllProjectMembers(projectid)
      .subscribe(members => this.members = members);
  }

  checkIfUserIsSpec() {
    for (var i=0; i < 3; i++) {
      if (this.task.spectators[i]) {
        if (this.task.spectators[i].id == this.ownUser.id) {
          this.spectator = true;
          console.log("User ist Beobachter");
        } else {
          this.spectator = false;
          console.log("User ist kein Beobachter");
        }
      } 
    }
  }

  saveTask() {

    const projectid = +this.route.snapshot.paramMap.get('projectid');
    const taskid = +this.route.snapshot.paramMap.get('taskid');
    
    if (this.spectator) {
      this.taskService.updateSpectateTask(projectid, taskid).subscribe();
      console.log("Beobachten");
    } else {
      this.taskService.updateSpectateTask(projectid, taskid).subscribe();
      console.log("Nicht Beobachten");
    }

    
    this.task.status.id = this.setStatus;
    this.task.priority.id = this.setPriority;
    this.task.responsible.id = this.setResponsible;
    console.log(this.task);
    /*this.getStatus();
    this.getPriorities();
    this.getTaskById();*/


    this.taskService.updateTaskById(projectid, taskid, this.task)
      .subscribe();

      setTimeout(() => {
        this.toggle = !this.toggle; //Bearbeitungsansicht schließen
        this.checkIfUserIsSpec();
        this.setPriority = this.task.priority.id;
        this.setStatus = this.task.status.id;
        this.setResponsible = this.task.responsible.id;
        this.getTaskById();
      },1000);
    

  }

  closeTask() {
    this.dialog.open(CloseTaskComponent, {data: {task: this.task}});
  }


  cusMail(adresse:String){
    
    adresse = this.project.customer.mail;
      //const dialogConfig = new MatDialogConfig();
      this.dialog.open(CustomerMailComponent, {data: { mai: adresse}});
  }
}
