import { Component, OnInit, ViewChild } from '@angular/core';
import {File, AttachmentService } from 'src/app/services/attachement.service';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { ReallyDeleteComponent } from 'src/app/shared/popups/really-delete/really-delete.component';
import { formArrayNameProvider } from '@angular/forms/src/directives/reactive_directives/form_group_name';
import { RequestOptions, ResponseContentType } from '@angular/http';
import { UserService } from 'src/app/services/user.service';
import { ActivatedRoute } from '@angular/router';



@Component({
  selector: 'app-taskattachment',
  templateUrl: './taskattachment.component.html',
  styleUrls: ['./taskattachment.component.css']
})
export class TaskattachmentComponent implements OnInit {
  selectedFile: File = null;
  files: File[];

  currentRole: String;
  currentUserId: Number;

  constructor(private dialog: MatDialog, 
    private userService: UserService,
    private attachService: AttachmentService,
    private route: ActivatedRoute) { 
    //UserRole (Admin or PM?)
    this.userService.getUser().subscribe(user => this.currentRole = user.roles[0].name);
    this.userService.getUser().subscribe(user => this.currentUserId = user.id); 

    //this.files = service.getAllFiles();

    this.getAttachmentByTaskId();
  }

  ngOnInit() {
  }

  fileToUpload: any;
  // File auswählen und Daten übergeben 
  onFileSelected(fileList: FileList){
    this.fileToUpload = fileList.item(0);
   }

  deleteAttachment(file : File){
   //Attachment Service muss noch eingebunden werden 
   const dialogConfig = new MatDialogConfig();
    this.dialog.open(ReallyDeleteComponent,dialogConfig);
   }
 
   addAttachment(){
    console.log(this.fileToUpload);
    const taskId = +this.route.snapshot.paramMap.get('taskid');

     this.attachService.addTaskFile(this.fileToUpload, taskId )
     .subscribe(data => {
       result => console.log(result)
     }, error => { console.log(error)});
   }

   getAttachmentByTaskId(){
    const taskId = +this.route.snapshot.paramMap.get('taskid');
    this.attachService.getFilesByTaskId(taskId)
      .subscribe(files => this.files = files);
   }

   get isAdmin(){
    //console.log(this.currentRole + 'currentUserRole');
    //console.log(this.currentUserId + 'currentUserId');
  
    if( this.currentRole === 'ADMIN'){
       return true;
    }else{
      return false; 
    }
 }

   /* @ViewChild('form') form;
   reset(){
    this.form.nativeElement.reset();
   } */

   
   /* downloadAttachment(propertyId:String, fileId:string){
        const url= '';
        const options = new RequestOptions({responseType: ResponseContentType.Blob});

        //Process File Downloaded
        this.http.get(url, options).subscribe(res => {
          const fileName = getFileNameFromResponseContentDisposition(res);
          saveFile(res.blob(), fileName);
   } */
   
 }


 //http://amilspage.com/angular4-file-download/