import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskattachmentComponent } from './taskattachment.component';

describe('TaskattachmentComponent', () => {
  let component: TaskattachmentComponent;
  let fixture: ComponentFixture<TaskattachmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaskattachmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskattachmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
