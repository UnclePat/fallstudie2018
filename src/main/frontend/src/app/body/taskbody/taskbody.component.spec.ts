import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskbodyComponent } from './taskbody.component';

describe('TaskbodyComponent', () => {
  let component: TaskbodyComponent;
  let fixture: ComponentFixture<TaskbodyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaskbodyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskbodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
