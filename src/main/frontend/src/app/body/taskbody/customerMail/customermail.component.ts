import { Component, OnInit, Inject } from '@angular/core';
import { SendMail, MailService } from 'src/app/services/email.service';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-customer-mail',
  templateUrl: './customermail.component.html',
  styleUrls: ['./customermail.component.css']
})
export class CustomerMailComponent implements OnInit {
newMail = new SendMail();
adresse = new FormControl();
mailContent = new FormControl();

  constructor(public dialogRef: MatDialogRef<CustomerMailComponent>,   
    @Inject(MAT_DIALOG_DATA) public data: any,
    private mailService: MailService) { }

  ngOnInit() {
  }

  sendMail(){
    this.newMail.content = this.mailContent.value;
    this.newMail.to = this.data.mai;
    this.newMail.subject = 'Planstry Kundeninfo'; 

    console.log(this.newMail);
    
     this.mailService.sendMail(this.newMail)
    .subscribe(
      result => console.log(result)
    ); 
  }


}
