import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerMailComponent } from './customermail.component';

describe('NewMailComponent', () => {
  let component: CustomerMailComponent;
  let fixture: ComponentFixture<CustomerMailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerMailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerMailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
