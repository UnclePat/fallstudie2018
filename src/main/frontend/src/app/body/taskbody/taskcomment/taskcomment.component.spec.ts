import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskcommentComponent } from './taskcomment.component';

describe('TaskcommentComponent', () => {
  let component: TaskcommentComponent;
  let fixture: ComponentFixture<TaskcommentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaskcommentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskcommentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
