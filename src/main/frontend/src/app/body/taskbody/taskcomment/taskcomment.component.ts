import { Component, OnInit } from '@angular/core';
import { Comment,CommentService } from 'src/app/services/comment.service';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { ReallyDeleteComponent } from 'src/app/shared/popups/really-delete/really-delete.component';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { TaskService } from 'src/app/services/task.service';
import { maybeQueueResolutionOfComponentResources } from '@angular/core/src/metadata/resource_loading';



@Component({
  selector: 'app-taskcomment',
  templateUrl: './taskcomment.component.html',
  styleUrls: ['./taskcomment.component.css']
})
export class TaskcommentComponent implements OnInit {
  
  comments: Comment[];

  currentRole: String;
  currentUserId: number;
  currentUser: any;

  constructor(private service: CommentService, 
    private dialog: MatDialog, 
    private fb: FormBuilder,
    private route: ActivatedRoute, private userService:UserService, private commentService: CommentService) {
    //this.comments = service.getAllComments();

    //UserRole (Admin or PM?)
    this.userService.getUser().subscribe(user => this.currentRole = user.roles[0].name);
    this.userService.getUser().subscribe(user => this.currentUserId = user.id); 

    this.userService.getUser().subscribe(user => this.currentUser = user);
   
    
    
   }


  ngOnInit() {

     this.getAllComments();

  }

  
  
  //title = new FormControl();
  description = new FormControl();
  comment = new Comment();
    
addComment(){
   this.comment.text = this.description.value;
   this.comment.user = this.currentUser;
   
   console.log(this.comment);
  
   const projectid = +this.route.snapshot.paramMap.get('projectid');
   const taskid = +this.route.snapshot.paramMap.get('taskid');

    this.commentService.addComment(this.comment, projectid, taskid)
    .subscribe(
      result => console.log(result)
    );
  
   //window.location.reload();
   setTimeout(() => {
    this.getAllComments();
  }, 250);
}

openDeleteComment(comment: Comment){
    // console.log(comment.id); //console.log(comment , projectid , taskid);
     const projectid = +this.route.snapshot.paramMap.get('projectid');
     const taskid = +this.route.snapshot.paramMap.get('taskid');

     this.dialog.open(ReallyDeleteComponent, { data: { com: comment, pro: projectid, task: taskid }  });
}

   getAllComments () {
    const projectid = +this.route.snapshot.paramMap.get('projectid');
    const taskid = +this.route.snapshot.paramMap.get('taskid');
    this.service.getAllComments(projectid, taskid)
      .subscribe(comments => this.comments = comments);
     
   }

   getAllCommentsById(){
    const projectId = +this.route.snapshot.paramMap.get('id');
    const taskId = +this.route.snapshot.paramMap.get('taskid');
    const commentId = +this.route.snapshot.paramMap.get('commentId');
    this.commentService.getAllCommentsById(projectId, taskId, commentId).subscribe(comment => this.comments = comment );
   }
   
   
  get isAdmin(){
   
 

      //console.log(this.currentRole + 'currentUserRole');
      //console.log(this.currentCom );
    
      if( this.currentRole === 'ADMIN'){
         return true;
      }else{
        return false; 
      }
   }

   refresh(): void {
    //window.location.reload();
}

}
