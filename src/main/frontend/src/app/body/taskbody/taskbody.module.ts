import { NgModule }             from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';


import { TaskoverviewComponent } from './taskoverview/taskoverview.component';
import { TaskattachmentComponent } from './taskattachment/taskattachment.component';
import { TaskcommentComponent } from './taskcomment/taskcomment.component';
import { MatFormFieldModule, MatInputModule, MatDatepickerModule, MatNativeDateModule, MatIconModule, MatCheckboxModule, MatDialogModule, MatCardModule, MatSelectModule } from '@angular/material';
import { AttachmentService } from 'src/app/services/attachement.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CloseTaskComponent } from 'src/app/shared/popups/close-task/close-task.component';
import { CustomerMailComponent } from './customerMail/customermail.component';

@NgModule({
    declarations: [
       TaskoverviewComponent,
       TaskattachmentComponent,
       TaskcommentComponent,
       CustomerMailComponent
      ],

      imports: [
        BrowserModule,
        MatFormFieldModule,
        MatInputModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatIconModule,
        MatCheckboxModule,
        MatDialogModule,
        MatCardModule,
        MatSelectModule,
        FormsModule,
        ReactiveFormsModule,
        
      ],

      exports: [
        TaskoverviewComponent,
        TaskattachmentComponent,
        TaskcommentComponent,
        CustomerMailComponent
      
      ],
      providers: [ AttachmentService],

      entryComponents: [
        CloseTaskComponent,
        TaskoverviewComponent,
        CustomerMailComponent
      ]

})
export class TaskbodyModule {}