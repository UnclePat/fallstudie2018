import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Http, HttpModule } from '@angular/http';
import { CoreModule} from './core/core.module';
import { AuthService } from './auth.service';

import { AppComponent } from './app.component';
import { CoreComponent } from './core/core.component';
import { BodyComponent } from './body/body.component';

import { AppRoutingModule } from './app-routing.module';
import { BodyModule } from './body/body.module';
import { MatCardModule, MatToolbarModule, MatSidenavModule, MatListModule, MatButtonModule, MatIconModule, MatTableModule, MatPaginatorModule, MatCheckboxModule, MatFormField, MatFormFieldModule, MatSnackBarModule, MatMenuModule, MatInputModule, MatDatepickerModule } from '@angular/material';
import { ChartsModule } from 'ng2-charts';
import { SharedModule } from './shared/shared.module';
import { PersonService } from './services/person.service';
import { ProjectService } from './services/project.service';
import { TaskService } from './services/task.service';
import { MailService } from './services/email.service';
import { InvoiceService } from './services/invoice.service';
import { StatisticService } from './services/statistic.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AttachmentService } from './services/attachement.service';
import { CommentService } from './services/comment.service';
import { LoginComponent } from './shared/popups/login/login.component';
import { UserService } from './services/user.service';



@NgModule({
  declarations: [
    AppComponent,
    CoreComponent,
    BodyComponent,
 

   
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    HttpClientModule,
    AppRoutingModule,
    CoreModule,
    BodyModule,
    SharedModule,
    MatPaginatorModule,
    MatTableModule,
    MatCardModule,
    MatToolbarModule, 
    MatSidenavModule,
    MatListModule,
    MatButtonModule,
    MatIconModule,
    ChartsModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatSnackBarModule,
    MatMenuModule, 
    MatInputModule,
    FormsModule,
    MatDatepickerModule,
    ReactiveFormsModule
  ],
  providers: [ 
    PersonService, 
    ProjectService, 
    AttachmentService,
    TaskService,
    MailService,
    InvoiceService,
    CommentService,
    StatisticService,
    AuthService,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
