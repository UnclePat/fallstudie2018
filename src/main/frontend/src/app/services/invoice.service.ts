import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Http, Headers } from "@angular/http";

export class Invoice{
        amount: number;
        billed: true;
        description: string;
        id: number;
        pricePerQuatity: number;
        project: {
          budget: number;
          customer: {
            enterprise: string;
            firstname: string;
            housenumber: string;
            id: number;
            lastname: string;
            mail: string;
            mobil: string;
            name: string;
            phone: string;
            place: string;
            roles: [
              {
                description: string;
                id: number;
                name: string
              }
            ];
            street: string;
            zipcode: string
          };
          enddate: string;
          id: number;
          lf: number;
          projectmanager: {
            enterprise: string;
            firstname: string;
            housenumber: string;
            id: number;
            lastname: string;
            mail: string;
            mobil: string;
            name: string;
            phone: string;
            place: string;
            roles: [
              {
                description: string;
                id: number;
                name: string
              }
            ];
            street: string;
            zipcode: string
          };
          startdate: string;
          status: {
            id: number;
            name: string
          };
          title: string
        };
        quatity: number
}

export class OtherExpanses {
  amount: number;	
  billed:	boolean;
  description:	string;
  id: number;
  pricePerQuatity:	number;
  quatity: number;
}

@Injectable()
export class InvoiceService {

    billPosts: Observable <OtherExpanses[]>;
    billPost: Observable <OtherExpanses>;

    private token = localStorage.getItem('access_token');
    private headersForTokenAPI = new Headers({ 'content-Type': 'application/json', "Authorization": "Bearer " + this.token });
    //private projectUrl = "./assets/json/projects.json";

    private getProjectBillsByIdUrl = "http://193.196.37.95:8123/projects/";     ///projects/{id}/otherExpanses
    private invoiceUrl = "http://193.196.37.95:8123/rechnung";


    constructor (private http: Http) {
    }


    getBillPostsByProjectID(projectId: number): Observable<OtherExpanses[]> {
        return this.http.get(this.getProjectBillsByIdUrl + projectId + '/otherExpanses?billed=false', { headers: this.headersForTokenAPI })
        .map(res => res.json());
    }

    addBillPost(projectId: number, otherExpanse: OtherExpanses) {
      return this.http.post (this.getProjectBillsByIdUrl + projectId + '/otherExpanses', otherExpanse, {headers: this.headersForTokenAPI})
        .map(res => res.json());
    }
    
    getInvoices(object){
        return this.http.post (this.invoiceUrl, object, {headers: this.headersForTokenAPI})
          .map(res => res.json());
    }
}
