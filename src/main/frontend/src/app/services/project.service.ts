import { Injectable } from "@angular/core";

import { Http, Headers, Response } from "@angular/http";

import { Observable, of } from 'rxjs';
import 'rxjs/add/operator/map';

export class Project{
        budget: number;
        description: String;
        customer: {
          enterprise: string;
          firstname: string;
          housenumber: string;
          id: number;
          lastname: string;
          mail: string;
          mobil: string;
          name: string;
          phone: string;
          place: string;
          roles: [
            {
              description: string;
              id: number;
              name: string
            }
          ];
          street: string;
          zipcode: string
        };
        enddate: string;
        id: number;
        lf: number;
        projectmanager: {
          enterprise: string;
          firstname: string;
          housenumber: string;
          id: number;
          lastname: string;
          mail: string;
          mobil: string;
          name: string;
          phone: string;
          place: string;
          roles: [
            {
              description: string;
              id: number;
              name: string
            }
          ];
          street: string;
          zipcode: string
        };
        startdate: string;
        status: {
          id: number;
          name: string
        };
        title: string
}

export class Status {
  id: number;
  name: string
}

@Injectable()
export class ProjectService {

    projects: Observable <Project[]>;
    project: Observable <Project>;

    private token = localStorage.getItem('access_token');
    private headersForTokenAPI = new Headers({ 'content-Type': 'application/json', "Authorization": "Bearer " + this.token });
    //private projectUrl = "./assets/json/projects.json";

    private getAllProjectsUrl = "http://193.196.37.95:8123/projects";
    private getProjectByIdUrl = "http://193.196.37.95:8123/projects/";
    private getProjectStatusUrl = "http://193.196.37.95:8123/projects/status";

    constructor (private http: Http) {
    }


    getStatus(): Observable <Status[]> {
        return this.http.get(this.getProjectStatusUrl, {headers: this.headersForTokenAPI})
          .map(res => res.json());
    }
    

    //alle Projekte
    getProjects(): Observable <Project[]> {
        this.projects = this.http.get(this.getAllProjectsUrl + "?status=offen", { headers: this.headersForTokenAPI })
            .map(res => res.json());
        return this.projects;
    }

    getProjectById (projectId:number): Observable <Project> {
        /*this.getProjects();
        this.project = this.projects
            .map (projects => projects.find(p => p.id == projectId));*/
        this.project = this.http.get(this.getProjectByIdUrl + projectId, { headers: this.headersForTokenAPI })
            .map(res => res.json());
        return this.project;
    }

    addProject(newProject: Project){
      console.log(newProject);

      return this.http.post(this.getAllProjectsUrl, newProject ,{ headers: this.headersForTokenAPI } )
      .map(res => res.json());
    }

    closeProjectById (projectId: number, project: Project) {
      return this.http.patch(this.getProjectByIdUrl + projectId, project, {headers: this.headersForTokenAPI});
    }

    //Seite wird mit dieser Funktion erneut geladen, nicht aus dem chache sondern komplett neu
 refresh(){
  window.location.reload();
  //return true;
}
}
