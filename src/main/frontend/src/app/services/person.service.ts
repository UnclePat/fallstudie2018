import { Injectable } from "@angular/core";
import { Observable, BehaviorSubject } from "rxjs";
import { Http, Headers} from "@angular/http";

export class Person {
        enterprise: string;
        firstname: string;
        housenumber: string;
        id: number;
        lastname: string;
        mail: string;
        mobil: string;
        name: string;
        phone: string;
        place: string;
        roles: [
          {
            description: string;
            id: number;
            name: string
          }
        ];
        street: string;
        zipcode: string
}

export class Member {
    id: number;
    projectId: number;
  }

    
@Injectable()
export class PersonService {

    members: Observable <Person[]>;
    member: Observable <Person>; 

    private token = localStorage.getItem('access_token');
    private headersForTokenAPI = new Headers({ 'content-Type': 'application/json', "Authorization": "Bearer " + this.token });

    private getMembersUrl = "http://193.196.37.95:8123/projects/"; //+ id/members

    
    constructor(private http: Http){
    }

    getAllProjectMembers(projectId: number): Observable <Person[]> {
       this.members = this.http.get(this.getMembersUrl + projectId +'/members', { headers: this.headersForTokenAPI })
            .map(res => res.json());
        return this.members;
    }

    addProjectMember(projectId: number, member: Person) {
        return this.http.post (this.getMembersUrl + projectId + "/members", member, {headers: this.headersForTokenAPI})
            .map(res => res.json());
    }

    deleteProjectMember(projectId: number, memberId: number) {
        return this.http.delete(this.getMembersUrl + projectId + "/members/" + memberId, {headers: this.headersForTokenAPI});
    }


    //funktioniert noch nicht!!!!!!!!!!!!
    /* getPeopleByProjectId(projectID:number[]) {
         return people.filter(x => [x.projectID] === projectID); 
    
    } */

    /* getPersonById(personID:number): Promise<Person>{
        return this.getAllPeople()
            .then(pepole => people.find(person => person.personID === personID));
    } */

    
        
        

        
    }
    
