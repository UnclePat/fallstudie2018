import { Injectable } from "@angular/core";
import { Http, Headers } from "@angular/http";
import { Observable } from "rxjs";

export class Comment{
        created: Date;
        edited: boolean;
        id: number;
        lf: number;
        text: string;
        user: {
          firstname: string;
          housenumber: string;
          id: number;
          lastname: string;
          mail: string;
          mobil: string;
          name: string;
          phone: string;
          place: string;
          roles: [
            {
              description: string;
              id: number;
              name: string
            }
          ];
          street: string;
          zipcode: string
        };
        visable: boolean
}

@Injectable()
export class CommentService {

    comments: Observable <Comment[]>;
    comment: Observable <Comment>; 

    private token = localStorage.getItem('access_token');
    private headersForTokenAPI = new Headers({ 'content-Type': 'application/json', "Authorization": "Bearer " + this.token });

    private getCommentsUrl = "http://193.196.37.95:8123/projects/"; //+ {{pID}}/tasks/{tId}/comments

    
    constructor(private http: Http){
    }



    getAllComments(projectId: number, taskId: number): Observable <Comment[]> {
        this.comments = this.http.get(this.getCommentsUrl + projectId +'/tasks/' + taskId + '/comments', { headers: this.headersForTokenAPI })
        .map(res => res.json());
    return this.comments;    
    }



  getAllCommentsById(projectId: number, taskId: number, commentId: number): Observable <Comment[]>{
    this.comments = this.http.get(this.getCommentsUrl + projectId +'/tasks/' + taskId + '/comments' + commentId, { headers: this.headersForTokenAPI })
        .map(res => res.json());
    return this.comments; 
  }

  addComment(comment:Comment, projectId: Number, taskId:Number){
   // console.log(comment ); console.log ( projectId ); console.log(taskId);
    return this.http.post(this.getCommentsUrl + projectId + '/tasks/' + taskId + '/comments', comment, { headers: this.headersForTokenAPI })
    .map(res => res.json());
  }

  deleteComment(comment:Comment, projectId: Number, taskId:Number){
   // console.log(comment.id ); console.log(projectId ); console.log(taskId );
    return  this.http.delete(this.getCommentsUrl + projectId +'/tasks/' + taskId + '/comments/' + comment.id, { headers: this.headersForTokenAPI });
  } 

}
