import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { Http,Headers } from "@angular/http";

export class Customer {
    bic: string;
    email: string;
    housenumber: string;
    iban: string;
    id: number;
    localCourt: string;
    location: string;
    name: string;
    //firstname: string;
    paymentterms: string;
    phone: string;
    street: string;
    taxPercentage: number;
    taxid: string;
    website: string;
    zipcode: string
}

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  customers: Observable <Customer[]>;
  customer: Observable <Customer>;

  private token = localStorage.getItem('access_token');
  private headersForTokenAPI = new Headers({ 'content-Type': 'application/json', "Authorization": "Bearer " + this.token });
  private getAllCustomersUrl = "http://193.196.37.95:8123/enterprises"; 
  private updateCustomerUrl = "http://193.196.37.95:8123/enterprises/"; //+ {{customerId}}
  private getCustomerURL = "http://193.196.37.95:8123/user/customers";

  constructor (private http: Http) {
  }

  getAllCustomers(): Observable <Customer[]> {
    this.customers = this.http.get(this.getAllCustomersUrl, {headers: this.headersForTokenAPI})
      .map(res => res.json());
    return this.customers; 
  }

  getCustomerById(customerId: number): Observable<Customer>{
    this.customer = this.http.get(this.getAllCustomersUrl + '/' + customerId, {headers: this.headersForTokenAPI} )
    .map(res => res.json());
    return this.customer;
  }

  getCustomers(){
   return this.http.get(this.getCustomerURL,  {headers: this.headersForTokenAPI})
    .map( res => res.json());
  }

  addCustomer(customer:Customer){
    console.log(customer);
    return this.http.post(this.getAllCustomersUrl, customer, {headers: this.headersForTokenAPI} )
    .map(res => res.json()); 
  }

  deleteCustomer(customer: Customer){
    //console.log('delete customer - service' + customer);
    return this.http.delete(this.getAllCustomersUrl + '/' + customer.id , { headers: this.headersForTokenAPI });
  }

  updateCustomer(customer: Customer, customerId: number) {
    return this.http.patch(this.updateCustomerUrl + customerId, customer, {headers: this.headersForTokenAPI});
  }


  
}
