import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Http,Headers } from "@angular/http";

export class Email {
    content: string;
    id: number;
    sender: string;
    subject: string;
    timestamp: Date;
}

export class SendMail {
    content: string;
    subject: string;
    to: string;
}

@Injectable()
export class MailService {

    mails: Observable <Email[]>;
    mail: Observable <Email>;
    mailId: number;

    private token = localStorage.getItem('access_token');
    private headersForTokenAPI = new Headers({ 'content-Type': 'application/json', "Authorization": "Bearer " + this.token });
    private MailsUrl = "http://193.196.37.95:8123/mail"; 

    constructor (private http: Http) {

    }
    
    getAllMails(): Observable <Email[]> {
        this.mails = this.http.get(this.MailsUrl, { headers: this.headersForTokenAPI })
           .map(res => res.json());
       return this.mails;
   }

   deleteMail(mail: Email){
       // console.log(mail.id + mail.subject + ' wurde gelöscht'); 
     return this.http.delete(this.MailsUrl + '/' + mail.id , { headers: this.headersForTokenAPI });
   }

   sendMail(mail: SendMail){
       //console.log(mail);
       return this.http.post(this.MailsUrl, mail, {headers: this.headersForTokenAPI} ); 
   }

 
}
