import { Injectable, OnInit } from '@angular/core';
import { Observable, BehaviorSubject, interval, Subscription, SubscribableOrPromise, Subscriber } from 'rxjs';
import { Http, Headers, Response } from '@angular/http';
import { Token, analyzeAndValidateNgModules } from '@angular/compiler';
import { ActivatedRouteSnapshot, ResolveEnd } from '@angular/router';
import { resolve } from 'q';
import { validateConfig } from '@angular/router/src/config';
import { browser } from 'protractor';
import { element } from '@angular/core/src/render3/instructions';
import { startWith, switchMap, timeout, catchError } from 'rxjs/operators';
import { httpFactory } from '@angular/http/src/http_module';




export class User {
    enterprise: string;
    firstname: string;
    housenumber: string;
    id: number;
    lastname: string;
    mail: string;
    email: string;
    mobil: string;
    name: string;
    username: string;
    phone: string;
    place: string;
    roles: [
      {
        description: string;
        id: number;
        name: string
      }
    ];
    street: string;
    zipcode: string;
    password: String; 
}

@Injectable({providedIn: 'root'})
export class UserService{

  private user: Observable <User>;
  private users: Observable <User[]>;
  private token: String = localStorage.getItem('access_token');
  //public currentUser: User;
  private ownUserUrl = "http://193.196.37.95:8123/user/me";
  private userUrl = "http://193.196.37.95:8123/user";


  private headersForTokenAPI = new Headers({ 'content-Type': 'application/json', "Authorization": "Bearer " + this.token });

  constructor(private http: Http) { 
  }

   
  addUser( user: User){
    console.log(user);
   // return this.http.post(this.userUrl, user );
   return this.http.post(this.userUrl, user,  { headers: this.headersForTokenAPI })
    .map(res => res.json()); 
  }

  //Eigene Userdaten abfragen
  getUser(): Observable<User>{
    
    var headersForTokenAPI = new Headers({ 'content-Type'    : 'application/json', "Authorization": "Bearer " + this.token });
    return this.http.get(this.ownUserUrl, { headers: headersForTokenAPI })
      .map (res => res.json());  
    //.map (res => this.currentUser = res.json());

  }

  //Alle User abfragen (adminusers Ansicht)
  getAllUsers(): Observable <User[]> {
    this.users = this.http.get(this.userUrl, { headers: this.headersForTokenAPI })
      .map (res => res.json());
    return this.users;
  }

  updateUser(user: User) {
    return this.http.patch(this.userUrl, user, {headers: this.headersForTokenAPI});

  }
  


  userById: Observable <User>;

  //Nur Daten eines einzelnen Users _ Endpunktfehlt ...überflüssig?
  getUserById(userId: number){
   
    this.userById = this.http.get(this.userUrl + '/' +  userId, {headers: this.headersForTokenAPI  })
        .map(res => res.json());

    return this.userById;

  }
  

  deleteUser(user: User){
    console.log('delete user - service' + user);
  
    return this.http.delete(this.userUrl + '/' + user.id , { headers: this.headersForTokenAPI });
  }
  
}

