import { Injectable } from '@angular/core';

import { Http, Headers } from "@angular/http";

import { Observable, of } from 'rxjs';
import 'rxjs/add/operator/map';

export class Timespent {
  description: string; 
  id: number; 
  task: {
    id: number
  }
  timespent: number; 
  timestamp: Date; 
  user: {
    id: number;
  }
}

@Injectable({
  providedIn: 'root'
})
export class TimespentService {

  private token = localStorage.getItem('access_token');
  private headersForTokenAPI = new Headers({ 'content-Type': 'application/json', "Authorization": "Bearer " + this.token });
  private zeitbuchungUrl = "http://193.196.37.95:8123/projects/"; // {{pID}}/tasks/{{tId}}/zeitbuchungen
  constructor(private http: Http) { }

  addTime(projectId: number, taskId: number, timespent: Timespent) {
    return this.http.post(this.zeitbuchungUrl + projectId + "/tasks/" + taskId + "/zeitbuchungen", timespent, {headers: this.headersForTokenAPI} )
      .map(res => res.json());
  }

}
