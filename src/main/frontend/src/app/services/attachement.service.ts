import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Project } from "./project.service";
import { Http, Headers, ResponseContentType } from "@angular/http";
import { Task } from "./task.service";

export class File {
    filename:string;
    id: number;
    project: Project;
    timestamp: string;
    task: Task;
    size: any;
    slice: any;


    //kann raus?
    dateiId:number;
    person:string;
    date: Date;
    projectId:number;
    taskId:number;

}


@Injectable()
export class AttachmentService {
    file: Observable <File>;
    files: Observable <File[]>;

    private token = localStorage.getItem('access_token');
    private headersForTokenAPI = new Headers({ 'content-Type': 'application/json', "Authorization": "Bearer " + this.token });
    private headersForTokenAPI2 = new Headers({  "Authorization": "Bearer " + this.token });
    private getAttachmentUrl = "http://193.196.37.95:8123/projects/"; //+ id/attachments
    private getTaskAttachmentUrl = "http://193.196.37.95:8123/task/";

    constructor(private http: Http){}

    getAllFiles() {
       return true;
    }

    getFilesByProjectId(projectId: number): Observable<File[]>{
        this.files = this.http.get(this.getAttachmentUrl + projectId + '/attachment', { headers: this.headersForTokenAPI })
            .map(res => res.json());
        return this.files;
    }

    getFilesByTaskId( taskId: number): Observable<File[]>{
        this.files = this.http.get(this.getTaskAttachmentUrl + taskId + '/attachment', { headers: this.headersForTokenAPI })
            .map(res => res.json());
        return this.files;
    }

    addFile(fileToUpload: any, id:number){
        //console.log(fileToUpload );
        const formData: FormData = new FormData();
        formData.append('file', fileToUpload);
        return this.http.post( this.getAttachmentUrl + id + '/attachment', formData, { headers: this.headersForTokenAPI2 } )
            .map (() => {return true;}, error =>{ console.log(error)});
    }

    addTaskFile(fileToUpload:any, id:number){
        console.log(fileToUpload );
        const formData: FormData = new FormData();
        formData.append('file', fileToUpload);
         return this.http.post( this.getTaskAttachmentUrl + id + '/attachment', formData, { headers: this.headersForTokenAPI2 } )
            .map (() => {return true;}, error =>{ console.log(error)}); 
    }

    downloadProjectFile(projectId:number, fileId: number){
        console.log(projectId, fileId);

       // , responseType: ResponseContentType.Blob
         return this.http.get(this.getAttachmentUrl + 'attachment/' + fileId,{ headers: this.headersForTokenAPI2, responseType: ResponseContentType.Blob } )
            .map (res => { return res.blob()})
            .subscribe(res => {
                console.log(res);
                var url = window.URL.createObjectURL(res);
                var a = document.createElement('a');
                a.href = url;
                a.download = 'planstryFile';
                a.click();
                setTimeout(() => {
                    window.URL.revokeObjectURL(url);
                }, 100);
            });
            /* .subscribe(res =>{
                console.log('start download', res);
            
                var url = window.URL.createObjectURL(res);
                var a = document.createElement('a');
                document.body.appendChild(a);
                a.setAttribute('style','display:none');
                a.href = url;
                a.click();
                window.URL.revokeObjectURL(url);
                a.remove();
            }, error => {
                console.log('download error', JSON.stringify(error));
            }, ()=>{
                console.log('completed file download')
            }); */
    
            //  {return new Blob([res.blob()], { type: 'application/pdf'}
    }
}