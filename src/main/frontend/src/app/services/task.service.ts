import { Injectable } from "@angular/core";

import { Http, Headers } from "@angular/http";

import { Observable, of } from 'rxjs';
import 'rxjs/add/operator/map';



export class Task {
  deadline: string;
  description: string;
  expectedExpense: number;
  id: number;
  lf: number;
  projectId: number;
  priority: {
    name: string;
    id: number;
  }
  responsible: {
    enterprise: string;
    firstname: string;
    housenumber: string;
    id: number;
    lastname: string;
    mail: string;
    mobil: string;
    name: string;
    phone: string;
    place: string;
    roles: [
      {
        description: string;
        id: number;
        name: string
      }
    ];
    street: string;
    zipcode: string
  };
  spectators: [
    {
      enterprise: string;
      firstname: string;
      housenumber: string;
      id: number;
      lastname: string;
      mail: string;
      mobil: string;
      name: string;
      phone: string;
      place: string;
      roles: [
        {
          description: string;
          id: number;
          name: string
        }
      ];
      street: string;
      zipcode: string
    }
  ];
  status: {
    name: string;
    id: number;
  }
  title: string
}

export class Priority {
  id: number;
  name: string
}

export class Status {
  id: number;
  name: string
}

@Injectable({ providedIn: 'root' })
export class TaskService {

  tasks: Observable<Task[]>;
  ownTasks: Observable<Task[]>;
  task: Observable<Task>;
  private token = localStorage.getItem('access_token');
  private headersForTokenAPI = new Headers({ 'content-Type': 'application/json', "Authorization": "Bearer " + this.token });
  private getAllTasksUrl = "http://193.196.37.95:8123/projects/"; //{ID}/tasks ///projects/{id}/tasks/{tId}/spectate
  private getTaskByIdUrl = "http://193.196.37.95:8123/projects/"; //{ID}/tasks/{tId}
  private getUserTasksWeekUrl = "http://193.196.37.95:8123/task";
  private getSpectatedTasksUrl = "http://193.196.37.95:8123/task/spectate/all";
  private getUserTasksWeekStatusUrl = "http://193.196.37.95:8123/task?status=";
  private getAllUserTasksUrl = "http://193.196.37.95:8123/user/"; //{userId}/responsible

  private getPriorityUrl = "http://193.196.37.95:8123/projects/tasks/priorities";
  private getStatusUrl = "http://193.196.37.95:8123/projects/tasks/status";

  constructor(private http: Http) {
  }


  /** GET tasks from the "server" */
  /*getTasks(): Observable <Task[]> {
       this.tasks = this.http.get(this.taskUrl)
          .map(res => res.json());
      return this.tasks;
  }*/
  getPriorities(): Observable<Priority[]> {
    return this.http.get(this.getPriorityUrl, { headers: this.headersForTokenAPI })
      .map(res => res.json());
  }

  getStatus(): Observable<Status[]> {
    return this.http.get(this.getStatusUrl, { headers: this.headersForTokenAPI })
      .map(res => res.json());
  }

  getTasks(projectId: number): Observable<Task[]> {
    this.tasks = this.http.get(this.getAllTasksUrl + projectId + "/tasks", { headers: this.headersForTokenAPI })
      .map(res => res.json());
    return this.tasks;
  }

  getTaskById(projectId: number, taskId: number): Observable<Task> {
    //this.getTasks();
    this.task = this.http.get(this.getTaskByIdUrl + projectId + "/tasks/" + taskId, { headers: this.headersForTokenAPI })
      .map(res => res.json());
    return this.task;
  }

  getTasksByStatus(projectid: number, status: string): Observable<Task[]> {
    this.tasks = this.http.get(this.getAllTasksUrl + projectid + "/tasks?status=" + status, { headers: this.headersForTokenAPI })
      .map(res => res.json());
    return this.tasks;
  }

  getTaskByStatusDeadline(projectid: number, status: String, until: number): Observable<Task[]> {
    return this.http.get(this.getAllTasksUrl + projectid + "/tasks?status=" + status + "&until=" + until, { headers: this.headersForTokenAPI })
      .map(res => res.json());
  }

  //Alle Tasks für die der User responsible ist
  getAllUserTasks(userId: number): Observable<Task[]> {
    return this.http.get(this.getAllUserTasksUrl + userId + "/responsible", { headers: this.headersForTokenAPI })
      .map(res => res.json());
  }

  //Tasks, die der User beobachtet
  getAllSpectatedTasks(): Observable <Task[]> {
    return this.http.get(this.getSpectatedTasksUrl, {headers: this.headersForTokenAPI})
      .map(res => res.json());
  }

  //Alle Tasks für die der User responsible ist & die in den nächsten 7 Tagen fällig sind, können nach Status abgefragt werden
  getAllUserTaksByStatus(status: string): Observable<Task[]> {
    return this.http.get(this.getUserTasksWeekStatusUrl + status, { headers: this.headersForTokenAPI })
      .map(res => res.json())
  }

  //Alle Tasks für die der User responsible ist, können nach Status & Fälligkeit "heute" abgefragt werden 
  getAllUserTasksDeadline(status: string): Observable<Task[]> {
    return this.http.get(this.getUserTasksWeekStatusUrl + status + "&until=0", { headers: this.headersForTokenAPI })
      .map(res => res.json())
  }

  //Home, Tasks von User
  getUserTasksThisWeek(): Observable<Task[]> {
    this.ownTasks = this.http.get(this.getUserTasksWeekUrl, { headers: this.headersForTokenAPI })
      .map(res => res.json());
    return this.ownTasks;
  }

  updateTaskById(projectId: number, taskId: number, task: Task) {
    return this.http.patch(this.getTaskByIdUrl + projectId + "/tasks/" + taskId, task, { headers: this.headersForTokenAPI });
  }

  addTask(task: Task, projectid: number) {
    return this.http.post(this.getAllTasksUrl + projectid + "/tasks", task, { headers: this.headersForTokenAPI })
      .map(res => res.json());
  }

  //projects/{id}/tasks/{tId}/spectate
  updateSpectateTask(projectId: number, taskId: number): Observable<any[]> {
    return this.http.get(this.getAllTasksUrl + projectId + "/tasks/" + taskId + "/spectate", { headers: this.headersForTokenAPI })
      .map(res => res.json());
  }

}
