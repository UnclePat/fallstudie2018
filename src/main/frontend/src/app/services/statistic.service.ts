import { Injectable } from "@angular/core";

export class Statistic {
    
    projectId: number;
    intime:number;
    exceed:number;
    amountOpenTasks:number;
    amountDoingTasks: number;
    amountDoneTasks: number;
    amountPersonOfProject:number;
    amountOpenInvoices: number;
    amountInvoices:number;
   

}

let statistics: Statistic [] =[{
    "projectId":1,
    "intime": 80,
    "exceed": 30,
    "amountOpenTasks": 11,
    "amountDoingTasks": 40,
    "amountDoneTasks": 20,
    "amountPersonOfProject": 7,
    "amountOpenInvoices": 50,
    "amountInvoices":50
},
{
    "projectId":2,
    "intime": 80,
    "exceed": 30,
    "amountOpenTasks": 11,
    "amountDoingTasks": 40,
    "amountDoneTasks": 20,
    "amountPersonOfProject": 7,
    "amountOpenInvoices": 50,
    "amountInvoices":50
}]

@Injectable()
export class StatisticService {
    getStatistics() {
        return statistics;
    }
}