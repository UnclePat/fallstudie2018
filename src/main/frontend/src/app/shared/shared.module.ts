import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChatComponent } from './chat/chat.component';
import { PopupsComponent } from './popups/popups.component';
import { PopupsModule } from './popups/popups.module';
import { ChatModule } from './chat/chat.module';
import { MatButtonModule, MatDialogContent, MatDialogModule,} from '@angular/material';
import { ReallyLogoutComponent } from './popups/really-logout/really-logout.component';
import { ReallyDeleteComponent } from './popups/really-delete/really-delete.component';
import { Http } from '@angular/http';
import { HttpClient } from 'selenium-webdriver/http';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  imports: [
    CommonModule,
    PopupsModule,
    ChatModule,
    BrowserModule,
    HttpClientModule
  ],
  declarations: [
    PopupsComponent,
   
  ],

  exports: [
    PopupsComponent,
  
  ]
})
export class SharedModule { }