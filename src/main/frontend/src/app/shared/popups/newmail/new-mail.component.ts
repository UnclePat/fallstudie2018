import { Component, OnInit, Inject } from '@angular/core';
import { SendMail, MailService } from 'src/app/services/email.service';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-new-mail',
  templateUrl: './new-mail.component.html',
  styleUrls: ['./new-mail.component.css']
})
export class NewMailComponent implements OnInit {
newMail = new SendMail();
adresse = new FormControl();
mailContent = new FormControl();

  constructor(public dialogRef: MatDialogRef<NewMailComponent>,   
    private mailService: MailService) { }

  ngOnInit() {
  }

  sendMail(){
    this.newMail.content = this.mailContent.value;
    this.newMail.to = this.adresse.value;
    this.newMail.subject = 'Planstry Info';

    console.log(this.newMail);
    
     this.mailService.sendMail(this.newMail)
    .subscribe(
      result => console.log(result)
    ); 
  }


}
