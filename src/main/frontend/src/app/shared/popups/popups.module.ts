import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ForgotPWComponent } from './forgot-pw/forgot-pw.component';
import { LoginComponent } from './login/login.component';
import { ReallyLogoutComponent } from './really-logout/really-logout.component';
import { ReallyDeleteComponent } from './really-delete/really-delete.component';
import { MatFormField, MatFormFieldModule, MatOptionModule, MatSelectModule, MatFormFieldControl, MatInputModule, MatChip, MatChipsModule, MatIcon, MatIconModule, MatCardImage, MatCardModule, MatDialogModule } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2Webstorage } from '@rars/ngx-webstorage';
import { BookingTimeComponent } from './booking-time/booking-time.component';
import { CloseProjectComponent } from './close-project/close-project.component';
import { AdditionalCostsComponent } from './additional-costs/additional-costs.component';
import { NewMailComponent } from './newmail/new-mail.component';
import { CloseTaskComponent } from './close-task/close-task.component';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    MatFormFieldModule,
    MatOptionModule,
    MatSelectModule,
    MatInputModule,
    MatChipsModule,
    MatIconModule,
    MatCardModule,
    MatDialogModule,
    HttpClientModule,
    FormsModule, 
    Ng2Webstorage,
    FormsModule,
    ReactiveFormsModule

  ],
  declarations: [
    ForgotPWComponent,
    LoginComponent,
    ReallyDeleteComponent,
    ReallyLogoutComponent,
    BookingTimeComponent,
    CloseProjectComponent,
    AdditionalCostsComponent,
    NewMailComponent,
    CloseTaskComponent
    
  ],
  exports: [
    ForgotPWComponent,
    LoginComponent,
    ReallyDeleteComponent,
    ReallyLogoutComponent, 
    BookingTimeComponent,
    NewMailComponent
  ]
})
export class PopupsModule { }
