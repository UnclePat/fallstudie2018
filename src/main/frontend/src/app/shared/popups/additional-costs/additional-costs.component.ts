import { Component, OnInit, Inject } from '@angular/core';
import { InvoiceService, OtherExpanses } from 'src/app/services/invoice.service';
import { FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-additional-costs',
  templateUrl: './additional-costs.component.html',
  styleUrls: ['./additional-costs.component.css']
})
export class AdditionalCostsComponent implements OnInit {

  newBillPost = new OtherExpanses();

  description = new FormControl('');
  quatity = new FormControl('');
  pricePerQuatity = new FormControl('');

  constructor(private invoiceService: InvoiceService,
    private route: ActivatedRoute,
    public dialogRef: MatDialogRef<AdditionalCostsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
  }

  save() {
    this.newBillPost.description = this.description.value;
    this.newBillPost.quatity = this.quatity.value;
    this.newBillPost.pricePerQuatity = this.pricePerQuatity.value;
    this.newBillPost.billed = false;

    this.invoiceService.addBillPost(this.data.project.id, this.newBillPost)
      .subscribe();
    
      setTimeout(() => {
        window.location.reload();
        }, 1);

  }

}
