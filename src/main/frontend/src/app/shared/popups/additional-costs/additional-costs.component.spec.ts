import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdditionalCostsComponent } from './additional-costs.component';

describe('AdditionalCostsComponent', () => {
  let component: AdditionalCostsComponent;
  let fixture: ComponentFixture<AdditionalCostsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdditionalCostsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdditionalCostsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
