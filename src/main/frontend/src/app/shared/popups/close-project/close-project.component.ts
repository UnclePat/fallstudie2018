import { Component, OnInit, Inject } from '@angular/core';
import { ProjectService, Status } from 'src/app/services/project.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-close-project',
  templateUrl: './close-project.component.html',
  styleUrls: ['./close-project.component.css']
})
export class CloseProjectComponent implements OnInit {

  constructor(private projectService: ProjectService,
    public dialogRef: MatDialogRef<CloseProjectComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { 
      this.projectService.getStatus().subscribe(status => this.projectStatus = status);    
    }

  projectStatus: Status[]; 

  ngOnInit() {
  }

  getProjectStatus() {
    this.projectService.getStatus()
      .subscribe(status => this.projectStatus = status);
  }


  closeProject() {
    console.log(this.projectStatus);
    for (var i=0; i < 3; i++) {
      if (this.projectStatus[i].name == "Geschlossen") {
        this.data.project.status = this.projectStatus[i];
      }
    }
    this.projectService.closeProjectById(this.data.project.id, this.data.project)
      .subscribe();
  }
}
