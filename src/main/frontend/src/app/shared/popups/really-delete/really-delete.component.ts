import { Component, OnInit, inject, Inject, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

import { isDeleteExpression } from 'typescript';
import { Action } from 'rxjs/internal/scheduler/Action';
import { MailService, Email } from 'src/app/services/email.service';
import { User, UserService } from 'src/app/services/user.service';
import { userInfo } from 'os';
import { Customer, CustomerService } from 'src/app/services/customer.service';
import { CommentService, Comment } from 'src/app/services/comment.service';
import { PersonService } from 'src/app/services/person.service';
import { TeamComponent } from 'src/app/body/projectbody/team/team.component';
import { TaskcommentComponent } from 'src/app/body/taskbody/taskcomment/taskcomment.component';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-really-delete',
  templateUrl: './really-delete.component.html',
  styleUrls: ['./really-delete.component.css']
})
export class ReallyDeleteComponent implements OnInit {
  user: User;

  constructor(
    private mailService:MailService,
    private userService: UserService,
    private customerService: CustomerService,
    private commentService: CommentService,
    private personService: PersonService,
    public dialogRef: MatDialogRef<ReallyDeleteComponent>,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) public data: any){
   
      console.log(this.data); 
    
      
      //hier werden die jeweils passenden Daten an die einzelnen Methoden übergeben
      

  }

  ngOnInit() {
  }

  


  public delete(data: any){
    //bei JA/Löschen klick -> testen ob das so funktionierne könnte?! 
    if (this.data.us != null){
      this.deleteUser(this.data.us);

    }else if (this.data.mai != null){
      this.deleteMail( this.data.mai);

    }else if( this.data.cus != null){
      this.deleteCustomer(this.data.cus);

    } else if (this.data.team != null){
      this.deleteTeamMember(this.data.team);

    } else if (this.data.com != null){
     this.deleteComment(this.data.com);

    } else {
      console.log('delete not possible - object undefined');
    }
    
  }

  

  // kann nur gelöscht werden, wenn keine Abhängigkeiten bestehen
   deleteUser(data: User){
    console.log('delete user ' + this.data.us  );

    this.userService.deleteUser(data);
    
    this.userService.deleteUser(data)
    .subscribe((data)=>{
      console.log('delete User successful');}, error => alert("Can't delete user due to existing dependencies")
    ); 
   /*  this.userService.deleteUser(data).subscribe((data)=>{
      console.log('delet User successful');
    }); */
    setTimeout(() => {
      window.location.reload();
      }, 1);
  }

  

   deleteMail(data: Email){    
    //console.log('delete Mail '+ this.data.mai.id );    
    
    this.mailService.deleteMail(data).subscribe((data)=>{
      console.log('delete Mail successful');
    });
    setTimeout(() => {
      window.location.reload();
      }, 1);
  }

  // kann nur gelöscht werden, wenn keine Abhängigkeiten bestehen
  deleteCustomer(data: Customer){
    //console.log('delete Customer '+ this.data.cus );

    this.customerService.deleteCustomer(data)
    .subscribe((data)=>{
      console.log('delete Customer successful');}, error => alert("Can't delete customer due to existing dependencies")
    );
    setTimeout(() => {
      window.location.reload();
      }, 1);
  }

  // kann nur gelöscht werden, wenn keine Abhängigkeiten bestehen
  deleteTeamMember(data: User){

    this.personService.deleteProjectMember(this.data.team.projectId, this.data.team.id)
    .subscribe((data)=>{
      console.log('delete Teammember successful');}, error => alert("Can't delete customer due to existing dependencies")
    );

    setTimeout(() => {
      window.location.reload();
      }, 1);
  }




  deleteComment(data: any){
    //console.log ('delete comment ' +  this.data.com, this.data.pro, this.data.task);

     this.commentService.deleteComment(this.data.com, this.data.pro, this.data.task)
     .subscribe(
       (response) => {console.log('comment deleted');}
     );  

     setTimeout(() => {
     window.location.reload();
     }, 1);
   // this.commentService.deleteComment(data);
  }

}
