import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
//import { TokenParams} from './Classes/TokenParams';
import { TokenParams } from '../../../Classes/TokenParams';
import { AuthService } from '../../../../app/auth.service';
import { Router } from '@angular/router';
import { Http, Response } from '@angular/http';
import { store } from '@angular/core/src/render3/instructions';
import { tokenKey } from '@angular/core/src/view';
import { Project, ProjectService } from 'src/app/services/project.service';
import { Headers, HttpModule } from '@angular/http';
import { Observable, from } from 'rxjs';

//import { refresh } from '../../../body/projectbody/overview';



import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { when } from 'q';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  hide = true;
  tokenParam: TokenParams;
  username: string;
  password: string;
  user: JSON;



  constructor(
    private http: Http,
    private router: Router,
    private projectService: ProjectService,
    private authService: AuthService) { }
  token: string;

  DoLoginGetRole() {
    this.DoLogin();
    setTimeout(() => {
      this.GetRole();
    //this.projectService.refresh();
    }, 150);
  
    //refresh.refresh();
  };

 
  

  DoLogin(): void {

    //this.user = JSON.parse('{"test":true}');


    this.authService.login(this.username, this.password)
      .subscribe(

        data => {
          this.tokenParam = data;
          this.authService.AccessToken = this.tokenParam.access_token;
         // this.router.navigate(['/']);
         //Hiermit wird die Home Seite refresht, nachdem der Login-Button gedrückt wurde. 
         //this.projectService.refresh() ruft die Methode auf: window.location.reload() damit alle Daten gefetched werden
       do(this.router.navigate(['/'])
         )
          while(
            this.projectService.refresh()
          );
          localStorage.setItem('access_token', data.access_token);
        }
      );        
  }



  GetRole() {

    //Hier wird der Token aus dem LocalStorage geladen und in der Variablen token gespeichert
    this.token = localStorage.getItem('access_token');
    //Der Token wird Testweise im Log dargestellt
    //console.log(this.token);
    var headersForTokenAPI = new Headers({ 'content-Type': 'application/json', "Authorization": "Bearer " + this.token });
    this.http.get('http://193.196.37.95:8123/user/me', { headers: headersForTokenAPI })
      //In dem this.user wird das JSON File des Benutzers gespeichert 
      .map((response: Response) => response.json()).subscribe(s => this.user = s);

    //this.user.roles[0].name beinhaltet die Bezeichnung / Rollenname des Users
    // if (this.user.roles[0].name == "ADMIN"){
    //   console.log(this.user.name + " ist Admin");
    // }

    console.log(this.user);
    //setTimeout(this.GetRole,700);
  }


  ngOnInit() {
  }

}