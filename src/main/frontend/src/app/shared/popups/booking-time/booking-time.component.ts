import { Component, OnInit, Inject } from '@angular/core';
import { Timespent, TimespentService } from 'src/app/services/timespent.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl } from '@angular/forms';
import { UserService, User } from 'src/app/services/user.service';

@Component({
  selector: 'app-booking-time',
  templateUrl: './booking-time.component.html',
  styleUrls: ['./booking-time.component.css']
})
export class BookingTimeComponent implements OnInit {
  ownUser: User;

  time = new FormControl();
  description = new FormControl();

  constructor(public dialogRef: MatDialogRef<BookingTimeComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private timeService: TimespentService,
    private userService: UserService) { 
      this.userService.getUser()
        .subscribe(user => this.ownUser = user);
    }



  ngOnInit() {
  }

  saveTime() {

    const newTimespent = new Timespent(); 


    newTimespent.timespent = this.time.value;
    newTimespent.description = this.description.value; 
    newTimespent.user = this.ownUser;
    newTimespent.task = this.data.task;

    this.timeService.addTime(this.data.task.projectId, this.data.task.id, newTimespent)
      .subscribe();

    console.log(newTimespent);
  }
}
