import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../auth.service';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { Location } from '@angular/common';


@Component({
  selector: 'app-forgot-pw',
  templateUrl: './forgot-pw.component.html',
  styleUrls: ['./forgot-pw.component.css']
})
export class ForgotPWComponent implements OnInit {

  constructor(
    private router: Router,
    private authService: AuthService,
    private location:Location) {}

    displaytoken: string;

  ngOnInit() {
    this.displaytoken = this.authService.AccessToken;
  }

  back(){
    this.location.back();
  }
}
