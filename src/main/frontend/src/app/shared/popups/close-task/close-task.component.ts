import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TaskService, Status } from 'src/app/services/task.service';

@Component({
  selector: 'app-close-task',
  templateUrl: './close-task.component.html',
  styleUrls: ['./close-task.component.css']
})
export class CloseTaskComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<CloseTaskComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private taskService: TaskService) {
      this.taskService.getStatus().subscribe(status => this.taskStatus = status);    

     }

  taskStatus: Status[]; 

  ngOnInit() {
  }

  closeTask() {
    console.log(this.taskStatus);
    for (var i=0; i < 3; i++) {
      if (this.taskStatus[i].name == "Geschlossen") {
        //this.projectStatus[i] = this.data.project.status;
        this.data.task.status = this.taskStatus[i];
        console.log(this.data.task.status.id);
      }
    }
    this.taskService.updateTaskById(this.data.task.projectId, this.data.task.id, this.data.task)
      .subscribe();
  }

}
