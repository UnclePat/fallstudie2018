import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth.service';
import { LoginComponent } from '../login/login.component';

@Component({
  selector: 'app-really-logout',
  templateUrl: './really-logout.component.html',
  styleUrls: ['./really-logout.component.css']
})
export class ReallyLogoutComponent implements OnInit {

  constructor(private router: Router, private authService: AuthService) { }

  ngOnInit() {
  }

  logout(){
    this.authService.logout();
    this.router.navigate(['/login']);

    setTimeout(() => {
      window.location.reload();
    }, 5);
  }
}
