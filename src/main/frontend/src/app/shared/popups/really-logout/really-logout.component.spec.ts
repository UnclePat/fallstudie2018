import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReallyLogoutComponent } from './really-logout.component';

describe('ReallyLogoutComponent', () => {
  let component: ReallyLogoutComponent;
  let fixture: ComponentFixture<ReallyLogoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReallyLogoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReallyLogoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
