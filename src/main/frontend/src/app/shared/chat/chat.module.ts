import { NgModule }             from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';


import { MatCardModule, MatToolbarModule, MatSidenavModule, MatListModule, MatButtonModule, MatIconModule, MatFormFieldModule, MatMenuModule, MatInputModule, MatTableModule, MatGridListModule} from '@angular/material';
import { ChatComponent } from './chat.component';
import { NewchatComponent } from './newchat/newchat.component';
import { AddpersonComponent } from './addperson/addperson.component';
import { ChatwindowComponent } from './chatwindow/chatwindow.component';

@NgModule({
    declarations: [
        ChatComponent,
        NewchatComponent,
        AddpersonComponent,
        ChatwindowComponent,
      ],

      imports: [
        MatCardModule,
        MatToolbarModule, 
        MatSidenavModule,
        MatListModule,
        MatButtonModule,
        MatIconModule,
        MatFormFieldModule,
        MatMenuModule,
        MatInputModule,
        MatTableModule,
        MatGridListModule,
        BrowserModule,
        BrowserAnimationsModule,
        RouterModule,
      ],

      exports: [
        ChatComponent,
        NewchatComponent,
        ChatwindowComponent
      ],
})
export class ChatModule {}