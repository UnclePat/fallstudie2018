import { Component, OnInit, Input } from '@angular/core';


@Component({
  selector: 'app-chatwindow',
  templateUrl: './chatwindow.component.html',
  styleUrls: ['./chatwindow.component.css']
})
export class ChatwindowComponent implements OnInit {

  @Input('matAutocompleteDisabled')
  autocompleteDisabled : true;

  constructor() { }

  ngOnInit() {
  }

}
