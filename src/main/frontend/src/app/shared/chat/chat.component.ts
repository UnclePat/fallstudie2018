import { Component, OnInit } from '@angular/core';


export interface openChats {
  name: string;
}

const ELEMENT_DATA: openChats[] = [
  { name: 'Lisa'}, 
  { name: 'Luisa'}, 
  { name: 'JD'}, 
  { name: 'Ascher'}, 
  { name: 'Marius'}, 
  { name: 'Moritz'}, 
  { name: 'Maren'}, 
 
  
];

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {  

  displayedColumns: string[] = ['name'];
  dataSource = ELEMENT_DATA;
  
  constructor() { }

  ngOnInit() {
  }

}
