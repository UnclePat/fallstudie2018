import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BodyComponent } from './body/body.component';
import { TaskoverviewComponent } from './body/taskbody/taskoverview/taskoverview.component';
import { HomeComponent } from './body/menubody/home/home.component';
import { StatisticsallComponent } from './body/menubody/statisticsall/statisticsall.component';
import { AdminmailsComponent } from './body/admin/adminmails/adminmails.component';
import { UsersetupsComponent } from './body/menubody/usersetups/usersetups.component';
import { TaskComponent } from './core/rightside/task/task.component';
import { ProjectComponent } from './core/rightside/project/project.component';
import { OverviewComponent } from './body/projectbody/overview/overview.component';
import { UsertasksComponent } from './body/menubody/usertasks/usertasks.component';
import { StatisticsprojectComponent } from './body/projectbody/statisticsproject/statisticsproject.component';
import { ReallyLogoutComponent } from './shared/popups/really-logout/really-logout.component';
import { TaskattachmentComponent } from './body/taskbody/taskattachment/taskattachment.component';
import { TaskcommentComponent } from './body/taskbody/taskcomment/taskcomment.component';
import { AttachmentComponent } from './body/projectbody/attachment/attachment.component';
import { InvoiceComponent } from './body/projectbody/invoice/invoice.component';
import { TeamComponent } from './body/projectbody/team/team.component';
import { LoginComponent } from './shared/popups/login/login.component';
import { ReallyDeleteComponent } from './shared/popups/really-delete/really-delete.component';
import { NewInvoiceComponent } from './body/projectbody/invoice/new-invoice/new-invoice.component';
import { ForgotPWComponent } from './shared/popups/forgot-pw/forgot-pw.component';
import { AdminusersComponent } from './body/admin/adminusers/adminusers.component';
import { AdminNewprojectComponent } from './body/admin/admin-newproject/admin-newproject.component';
import { NewUserComponent } from './body/admin/adminusers/new-user/new-user.component';
import { CloseProjectComponent } from './shared/popups/close-project/close-project.component';
import { AdditionalCostsComponent } from './shared/popups/additional-costs/additional-costs.component';
import { ChangeUserComponent } from './body/admin/adminusers/change-user/change-user.component';
import { AuthGuard } from './auth.guard';
import { AdmincompaniesComponent } from './body/admin/admincompanies/admincompanies.component';
import { NewcompanyComponent } from './body/admin/newcompany/newcompany.component';
import { ChangecompanyComponent } from './body/admin/changecompany/changecompany.component';
import { CustomerMailComponent } from './body/taskbody/customerMail/customermail.component';


const routes: Routes = [

  { path: 'login', component:LoginComponent},
  
  { path: '',  component: HomeComponent, canActivate:[AuthGuard] }, //, canActivate:[AuthGuard]-> hinter alle einfügen, die Login benötigen!!
  { path: 'body',  component: BodyComponent ,canActivate:[AuthGuard] },
  { path: 'home', component:HomeComponent ,canActivate:[AuthGuard] },
  { path: 'statisticsall', component:StatisticsallComponent ,canActivate:[AuthGuard]},
  { path: 'usersetups', component: UsersetupsComponent ,canActivate:[AuthGuard]},
  { path: 'usertasks', component: UsertasksComponent ,canActivate:[AuthGuard]},
  { path: 'newinvoice', component: NewInvoiceComponent ,canActivate:[AuthGuard]},
  { path: 'closeproject', component: CloseProjectComponent ,canActivate:[AuthGuard]},
  { path: 'task',  component: TaskComponent, outlet: 'rightside' ,canActivate:[AuthGuard]},
  { path: 'project',  component: ProjectComponent, outlet: 'rightside' ,canActivate:[AuthGuard] },

  //task
  { path: 'taskoverview/:projectid/:taskid', component: TaskoverviewComponent ,canActivate:[AuthGuard] }, 
  { path: 'taskattachment/:projectid/:taskid', component:TaskattachmentComponent ,canActivate:[AuthGuard]},
  { path: 'taskcomment/:projectid/:taskid', component:TaskcommentComponent ,canActivate:[AuthGuard]},

  //project 
  { path: 'statisticsproject/:projectid', component:StatisticsprojectComponent ,canActivate:[AuthGuard] },
  { path: 'projectattachment/:projectid', component:AttachmentComponent ,canActivate:[AuthGuard]},
  { path: 'invoice/:projectid', component:InvoiceComponent ,canActivate:[AuthGuard]},
  { path: 'team/:projectid', component:TeamComponent ,canActivate:[AuthGuard]},
  { path: 'projectoverview/:projectid', component:OverviewComponent ,canActivate:[AuthGuard]},
  //{ path: 'projectoverview', component:OverviewComponent},
 

  //admin 
  { path: 'adminmails', component: AdminmailsComponent ,canActivate:[AuthGuard]}, //data: {allowedRoles: ['admin']} oder data: {roles: [Role.Admin]} -> bei allen einfügen, die nur bestimmte Rollen erlauben
  { path: 'adminusers', component: AdminusersComponent ,canActivate:[AuthGuard]},
  { path: 'admin-newproject', component: AdminNewprojectComponent ,canActivate:[AuthGuard]},
  { path: 'admincompanies', component: AdmincompaniesComponent ,canActivate:[AuthGuard]},

  //popups
  { path: 'newcompany', component: NewcompanyComponent ,canActivate:[AuthGuard]},
  { path: 'changecompany', component: ChangecompanyComponent ,canActivate:[AuthGuard]},
  { path: 'newuser', component: NewUserComponent ,canActivate:[AuthGuard]},
  { path: 'reallyDelete', component: ReallyDeleteComponent ,canActivate:[AuthGuard]},  
  { path: 'forgotpw', component: ForgotPWComponent},
  { path: 'reallylogout', component: ReallyLogoutComponent ,canActivate:[AuthGuard]}, 
  {path: 'customemail', component: CustomerMailComponent, canActivate:[AuthGuard]}
]

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})

export class AppRoutingModule { }