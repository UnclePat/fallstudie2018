import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";

import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { Headers, Http, Response, HttpModule } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';

import { TokenParams } from './Classes/TokenParams';
import { LocalStorageService } from '@rars/ngx-webstorage';
import { strictEqual } from 'assert';
import { User } from './services/user.service';

@Injectable({providedIn:'root'})
export class AuthService{
    AccessToken: string="";
    constructor(private http: Http){}
    private TokenAPI = 'http://193.196.37.95:8123/oauth/token';

    

    login(Username: string, Password: string): Observable<TokenParams>{
        var headersForTokenAPI = new Headers({'content-Type': 'application/x-www-form-urlencoded', "Authorization": "Basic bXljbGllbnQ6c2VjcmV0"});
        var data = "grant_type=password&scope=read+write&username=" + Username + "&password=" + Password;

        return this.http.post(this.TokenAPI, data, { headers: headersForTokenAPI})
            .map(res => res.json());


    }
   
    logout(){
           //remove user from local storage
           localStorage.removeItem('access_token');
         
            //Test: Logout funktioniert (access_token = null)
            console.log(localStorage.getItem('access_token'),'Ausgeloggt');

    }  



    reloadPage(){
        location.reload();
    }


    reloadePageAfter() {
        this.logout();
        setTimeout(() => {
          this.reloadPage();
        }, 1000);
      
      };
    
} 


