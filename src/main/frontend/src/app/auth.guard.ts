import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

//used to prevent unauthenticated users from accessing restricted routes 


// WICHTIG!!!!!!! muss unter app-routing für alle Routen eingetragen werden WICHTIG!!!!



@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {

    constructor(private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (localStorage.getItem('access_token')) {

            //Testausgabe
            //console.log(localStorage.getItem('access_token'),'canActivate = true');

            // logged in so return true
            return true;

        }

        // not logged in so redirect to login page with the return url
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
        return false;
    }

    
    
}