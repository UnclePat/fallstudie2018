import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-rightside',
  templateUrl: './rightside.component.html',
  styleUrls: ['./rightside.component.css']
})
export class RightsideComponent implements OnInit {

  public isViewable: boolean;

  /** Life Cycle hook to initialize values */
	ngOnInit() { this.isViewable = true;}
  
	/** Simple method to toggle element visibility */
	public toggle(): void { this.isViewable = !this.isViewable; }
  constructor() { }

}
