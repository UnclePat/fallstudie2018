import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params, Routes } from '@angular/router';
import { OverviewComponent } from '../../../body/projectbody/overview/overview.component';
import { Project, ProjectService } from '../../../services/project.service';




@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css'],
  providers: [ProjectService],
})

export class ProjectComponent implements OnInit {
  project: Project;
  sub: any;
  id: any;
  animal: string;
  projectid: any;
  url: any;

  constructor( 
    private router: Router,
    private route: ActivatedRoute) { 
  }

  ngOnInit() {
  }

  getProjectIdFromUrl() {
    const url1 = this.router.url.split("/");
    const url2 = url1[2].split("(");
    this.projectid = url2[0];
  }
  openOverview() {
    this.getProjectIdFromUrl();
    this.router.navigate([{outlets: {primary: ['projectoverview', this.projectid], rightside: 'project'}}]);
  }

  openTeam() {
    this.getProjectIdFromUrl();
    this.router.navigate([{outlets: {primary: ['team', this.projectid], rightside: 'project'}}]);
  }

  openInvoice() {
    this.getProjectIdFromUrl();
    this.router.navigate([{outlets: {primary: ['invoice', this.projectid], rightside: 'project'}}]);
  }
  
  openStatistics() {
    this.getProjectIdFromUrl();
    this.router.navigate([{outlets: {primary: ['statisticsproject', this.projectid], rightside: 'project'}}]);
  }

  openAttachments() {
    this.getProjectIdFromUrl();
    this.router.navigate([{outlets: {primary: ['projectattachment', this.projectid], rightside: 'project'}}]);
  }
}
