import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

export interface taskButtons {
  nameIcon: string;
  description: string; 
}

const ELEMENT_DATA: taskButtons[] = [
  { nameIcon: 'apps', description: 'Übersicht'}, 
  { nameIcon: 'question_answer', description: 'Kommentare'}, 
  { nameIcon: 'attach_file', description: 'Anhänge'}, 
];

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {

  displayedColumns: string[] = ['nameIcon', 'description'];
  dataSource = ELEMENT_DATA;

  projectid: any; 
  taskid: any; 

  constructor(private route: ActivatedRoute,
    private router: Router ) { }

  ngOnInit() {
  }

  getIdsFromUrl() {
    const url1 = this.router.url.split("/");
    this.projectid = url1[2];
    const url2 = url1[3].split("(");
    this.taskid = url2[0];
  }

  openComments() {
    this.getIdsFromUrl();
    this.router.navigate([{outlets: {primary: ['taskcomment', this.projectid, this.taskid], rightside: 'task'}}]);
  }
  
  openOverview() {
    this.getIdsFromUrl();
    this.router.navigate([{outlets: {primary: ['taskoverview', this.projectid, this.taskid], rightside: 'task'}}]);
  }

  openAttachments() {
    this.getIdsFromUrl();
    this.router.navigate([{outlets: {primary: ['taskattachment', this.projectid, this.taskid], rightside: 'task'}}]);
  }
}
