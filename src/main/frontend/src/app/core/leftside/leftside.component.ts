import { Component, OnInit } from '@angular/core';
import {Http} from '@angular/http'; 
import { Observable, Subject } from 'rxjs';
import { map, filter, switchMap, tap } from 'rxjs/operators';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { Project, ProjectService } from 'src/app/services/project.service';
import { ActivatedRoute } from '@angular/router';
import { TaskService, Task } from 'src/app/services/task.service';
//import { merge } from 'rxjs';


@Component({
  selector: 'app-leftside',
  templateUrl: './leftside.component.html',
  styleUrls: ['./leftside.component.css']
})

export class LeftsideComponent implements OnInit {
  
  displayedColumns: string[] = ['project'];
  dataSource: any;
  private _refreshNeeded$ = new Subject<void>();

  get refreshNeeded$(){
    return this._refreshNeeded$;
  }
  
  projects: Project [];
  token:String = localStorage.getItem('access_token');

  constructor(
    private projectService: ProjectService,
    private route: ActivatedRoute,
    private taskService: TaskService) { 
  }

  ngOnInit() {
    this.getProjects();
  }

  getProjects(): void {
    this.projectService.getProjects()
      .subscribe((data) => {this.dataSource = new MatTableDataSource(data);}
      ) 
  }
  get userIsLoggedIn(){
    if (this.token != null){
     // console.log('token vorhanden');
      return true;
    }
   // console.log('kein token');
    return false;
  }
}
