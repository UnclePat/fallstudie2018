import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { ReallyLogoutComponent } from 'src/app/shared/popups/really-logout/really-logout.component';

@Component({
  selector: 'app-adminmenu',
  templateUrl: './adminmenu.component.html',
  styleUrls: ['./adminmenu.component.css']
})
export class AdminmenuComponent implements OnInit {
  public search: boolean;

    /** Life Cycle hook to initialize values */
	ngOnInit() { this.search = true;}
  
	/** Simple method to toggle element visibility */
  public toggle(): void { this.search = !this.search; }
  
  constructor(public dialog: MatDialog) { }

  openDialog(){
    const dialogConfig = new MatDialogConfig();
    this.dialog.open(ReallyLogoutComponent, dialogConfig);
  }
}
