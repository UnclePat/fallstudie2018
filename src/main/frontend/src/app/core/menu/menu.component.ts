import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { ReallyLogoutComponent } from 'src/app/shared/popups/really-logout/really-logout.component';
import { Router, ɵROUTER_PROVIDERS, ActivatedRoute } from '@angular/router';

import { LoginComponent } from 'src/app/shared/popups/login/login.component';
import { AuthService } from 'src/app/auth.service';
import { Observable } from 'rxjs';
import { User, UserService } from 'src/app/services/user.service';
import { ROUTER_PROVIDERS } from '@angular/router/src/router_module';
import { __String } from 'typescript';
import { LocalStorage } from '@rars/ngx-webstorage';
import { AuthGuard } from 'src/app/auth.guard';


@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
  providers: [UserService]
 
})
export class MenuComponent implements OnInit {

  //public search: boolean;

  /** Life Cycle hook to initialize values */
  ngOnInit() { /* this.search = true; */
  }
  
	/** Simple method to toggle element visibility */
  //public toggle(): void { this.search = !this.search; }
  
 
  currentRole: String;
  token:String = localStorage.getItem('access_token');

  constructor(public dialog: MatDialog, private router:Router, private userService: UserService,) {

      this.userService.getUser().subscribe(user => this.currentRole = user.roles[0].name);
   
  }

  get userIsLoggedIn(){
    if (this.token != null){
     // console.log('token vorhanden');
      return true;
    }
   // console.log('kein token');
    return false;
  }
  
 
  openDialog(){
    const dialogConfig = new MatDialogConfig();
    this.dialog.open(ReallyLogoutComponent, dialogConfig);
  }



get isAdmin(){    
    if(
        this.currentRole == null || this.currentRole != 'ADMIN'
      ){
        return false;

      } else {
           
            return true;
        }     
}        


}

export class CoreModule {}
