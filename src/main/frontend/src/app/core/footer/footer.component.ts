import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  token: String = localStorage.getItem('access_token');

  constructor() { }

  ngOnInit() {
  }

  get userIsLoggedIn(){
    if (this.token != null){
      //console.log('token vorhanden');
      return true;
    }
   // console.log('kein token');
    return false;
  }
}
