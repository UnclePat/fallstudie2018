import { NgModule }             from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';

import { FooterComponent } from './footer/footer.component';
import { MenuComponent } from './menu/menu.component';
import { LeftsideComponent } from './leftside/leftside.component';
import { RightsideComponent } from './rightside/rightside.component';


import { MatCardModule, MatToolbarModule, MatSidenavModule, MatListModule, MatButtonModule, MatIconModule, MatFormFieldModule, MatMenuModule, MatInputModule, MatTableModule, MatDialog, MatDialogModule, MatTooltipModule} from '@angular/material';
import { TaskComponent } from './rightside/task/task.component';
import { ChatModule } from '../shared/chat/chat.module';
import { AdminmenuComponent } from './adminmenu/adminmenu.component';
import { ProjectComponent } from './rightside/project/project.component';

@NgModule({
    declarations: [
        FooterComponent,
        MenuComponent,
        LeftsideComponent,
        RightsideComponent,
        TaskComponent,
        AdminmenuComponent,
        ProjectComponent
      ],

      imports: [
        MatCardModule,
        MatToolbarModule, 
        MatSidenavModule,
        MatListModule,
        MatButtonModule,
        MatIconModule,
        MatFormFieldModule,
        MatMenuModule,
        MatInputModule,
        MatTableModule,
        BrowserModule,
        BrowserAnimationsModule,
        RouterModule,
        ChatModule,
        MatDialogModule,
        MatTooltipModule
      ],

      exports: [
        FooterComponent,
        MenuComponent,
        LeftsideComponent,
        RightsideComponent,
        AdminmenuComponent,
        ProjectComponent
      ],
})
export class CoreModule {}