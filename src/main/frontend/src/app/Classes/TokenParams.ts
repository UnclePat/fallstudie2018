export class TokenParams{
   public access_token: string;
    token_type: string;
    expires_in:string;
    userName: string;
}