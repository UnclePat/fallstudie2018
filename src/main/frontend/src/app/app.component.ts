import { Component, OnInit } from '@angular/core';
import { RouterModule, Router, NavigationEnd } from '@angular/router';

export interface Tile {
  color: string;
  cols: number;
  rows: number;
  text: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'frontend';
  hideElement = false;
  hideLeftSide = false;
 private token : String = localStorage.getItem('acces_token');

/* Hier die Seiten hinzufügen, die kein rightsidebar enthalten sollen */
  constructor(private router: Router) {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        if (event.url.includes('/home')  || 
            event.url.includes ('mail') || 
            event.url.includes ('statisticsall')|| 
            event.url.includes ('usertasks') || 
            event.url.includes ('usersetups') || 
            event.url.includes ('reallylogout') ||
            event.url.includes ('admincompanies') ||
            event.url.includes ('adminusers') ||
            event.url.includes ('admin-newproject') ||
            event.url.includes ('login')) { 
          this.hideElement = true;
        }  else {
        this.hideElement = false;
        }
      }
    });
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        if (event.url.includes ('login')) { 
          this.hideLeftSide = true;
        }  else {
        this.hideLeftSide = false;
        }
      }
    });
  }

get returnToken(){

  if(this.token != null ){
    
    console.log('token not null');
    return false;
  }

  console.log('token null');
  return true;


}


}



